/*
Navicat MySQL Data Transfer

Source Server         : 本机数据库
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : cms

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-07-27 15:05:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for edms_article
-- ----------------------------
DROP TABLE IF EXISTS `edms_article`;
CREATE TABLE `edms_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT '内容页面父ID',
  `uid` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '标题',
  `keywords` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '内容描述',
  `image` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '图片路径',
  `content` text CHARACTER SET utf8 NOT NULL COMMENT '内容',
  `pv` int(11) NOT NULL DEFAULT '0' COMMENT '访问次数',
  `source` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '文章来源',
  `source_url` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '文章来源URL',
  `recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐 0默认 1推荐 ',
  `hot` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否热门 0默认  1热门',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0待审核, 1已审核',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_at` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='内容存储';

-- ----------------------------
-- Records of edms_article
-- ----------------------------
INSERT INTO `edms_article` VALUES ('1', '3', '1', '雄安新区重振“华北之肾”：将带动5千亿治水市场', '习近平', '新华社北京4月3日电 4月3日，在对芬兰共和国进行国事访问前夕，国家主席习近平在芬兰《赫尔辛基时报》发表题为《穿越历史的友谊》的署名文章。', 'upload/image/20170422/35c94bdb005969cec4ef8e333118d955.jpg', '<p>4月，河北雄安新区。暖风吹动着白洋淀，冬季残留的芦苇随风摇曳，泥水中新芽正在吐露，带来了春天的气息。</p><p>4月1日，新华社受权发布的一则消息打破这片湖泊的宁静：雄安新区将是继深圳经济特区和上海浦东新区之后又一具有全国意义的新区。</p><p>白洋淀恰恰是雄安新区的核心水系，地处京、津、石腹地，约366平方公里的水域内143个淀泊星罗棋布，3700条沟濠纵横交错，是华北大平原上的最大淡水湖，曾被称为“华北明珠”和“华北之肾”。白洋淀分属保定市的安新、雄县、容城、高阳以及沧州市的任丘五个县管辖，其中85%的水域在保定市安新县境内，将来白洋淀会成为雄安新区的水源地之一。</p><p>“雄安新区生态环境优良、资源环境承载力较强，拥有华北平原最大的淡水湖白洋淀等。水资源比较丰富，可满足区域生态用水需求。”国家发展改革委主任何立峰认为，雄安新区现有开发程度较低，发展空间比较充裕，具备高起点高标准开发建设的基本条件。</p><p>水城共融犹如江南水乡，大量管廊地下藏，地底通道汽车穿梭忙，行人休闲走在马路上，街道两边传统特色建筑分外亮堂，河水穿城流淌，森林公园空气清新舒畅，被绿树隔离带包围的白洋淀碧波荡漾……京津冀协同发展专家咨询委员会组长、中国工程院主席团名誉主席徐匡迪院士这样描述未来雄安新区美丽如画的模样，崭新的生产、生活、生态三大发展空间让人无限向往。</p><p>不过，白洋淀缺水且水质不佳的现状已持续多年，其能否担起重任，作为水源地之一满足将来涉及2000平方公里地区的生产、生活及工业用水需要，令人关注。</p><p>白洋淀面临的各种问题早已引起了国家和河北省相关部门的注意。2月23日，习近平总书记在实地考察雄安新区建设规划时专程前往白洋淀。习近平在考察中强调，建设雄安新区，一定要把白洋淀修复好、保护好。将来城市距离白洋淀这么近，应该留有保护地带。要有严格的管理办法，绝对不允许往里面排污水，绝对不允许人为破坏。</p><p>与此同时，有关白洋淀治理的相关规划也在部署。何立峰提出，要高标准高质量组织编制白洋淀生态环境治理和保护规划。目前，规划正在有序推进，环境保护行动已经提速。</p><p></p><p>有券商机构分析称，白洋淀生态流域的治理也将会大大带动环保产业的发展。国信证券环保团队通过测算得出，未来华北区域调水、流域生态和水污染治理等治水市场的投资将达到5000亿元级别。</p><p><br></p>', '521', '', '', '0', '0', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('2', '3', '1', '四川测试自贸区成都片区：6大方面落地 155项试验任务', '习近平', '新华社北京4月3日电 4月3日，在对芬兰共和国进行国事访问前夕，国家主席习近平在芬兰《赫尔辛基时报》发表题为《穿越历史的友谊》的署名文章。', 'upload/image/20170422/b5ac66691e68f7a07336a71de40e319e.jpg', '四川自贸试验区挂牌与奋力建设国家中心城市进入关键期，更意味着成都机遇叠加。在该负责人看来，作为联结“一带一路”、长江经济带和西部大开发国家战略的支点城市，成都需要“立足内陆、承东启西，服务全国、面向世界”，自觉担负起国家中心城市的责任，以做大做强西部经济中心、科技中心、文创中心、对外交往中心和综合交通枢纽功能建设西部开发开放门户城市；以成渝城市群和成渝经济区建设引领内陆开放战略支撑带；以国际航空和铁路枢纽建设织密高水平国际开放通道；以对接国际高标准经贸规则和高水平开放体系，扩大双向投资贸易，提升创新驱动能级，打造内陆开放型经济新高地；以实施“蓉欧＋”战略为统领，扩大与国内主要经济区域、口岸城市及交通节点互联互通深度合作，实现内陆与沿海沿边沿江协同开放。“力争通过3~5年的改革探索，将自贸试验区建成法制环境规范、投资贸易便利、创新要素集聚、监管高效便捷、协同开放效果显著的高水平高标准自由贸易园区。”该负责人表示。', '236', '', '', '0', '1', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('3', '5', '1', '网约车新政追踪：测试一纸公文引发市场新一轮洗牌？', '', '网约车新政追踪：一纸公文引发市场新一轮洗牌？', 'upload/image/20170422/06ab2d366d48a92f370c20ab6e823126.jpg', '随着网约车新政的颁布，有关部门对网约车的查处力度再次加大。就在几天前，拉了两年易到的周师傅在北京西客站遇到了便衣执法查车的工作人员，由于外地车牌和非京籍身份不符合北京市网约车新政细则的规定，周师傅的车被执法人员扣下，并因此被罚了款。', '275', '', '', '0', '1', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('4', '3', '1', '习近平“4·19”重要讲话一周年：让互联网更好造福人民', '金莲', '33', 'upload/image/20170422/6df76c2a4591e015d814d1ade1de6a7c.jpg', '<p>新华社北京4月18日电 题：让互联网更好造福人民——写在习近平总书记“4·19”重要讲话发表一周年之际</p><br><p>新华社记者 胡浩、施雨岑、王思北、李亚红</p><br><p>这是一个新家园，是学习、工作、生活的新空间，也是获取公共服务的新平台。</p><br><p>这是一股新动力，代表新生产力、新发展方向，在践行新发展理念上先行一步。</p><br><p>一年前，习近平总书记在网络安全和信息化工作座谈会上发表重要讲话，审时度势、高瞻远瞩，勾勒出中国网信战略的宏观框架，明确了中国网信事业肩负的历史使命，为深入推进网络强国战略指明了前进方向，为国际互联网治理提供了重要遵循。</p><br><p>党的十八大以来，特别是近一年来，我国多措并举、多方参与，开创出互联网发展、治理的新局面，实现了网信事业发展的新跨越，更好地推动经济社会发展，造福国家和人民。</p><br><p>代表新生产力、新发展方向，为新常态注入新动力</p><br><p>“我国经济发展进入新常态，新常态要有新动力，互联网在这方面可以大有作为。”一年前，习近平总书记强调，网信事业代表着新的生产力、新的发展方向，应该也能够在践行新发展理念上先行一步。</p><br><p>传统企业在互联网时代也能有很好的发展空间。青岛红领集团本是传统的服装企业，近年来，借助信息化手段，企业推出全球服装定制供应商平台，实现了在大流水线上定制生产个性化产品、满足客户的个性化需求，企业收入大幅提升。</p><br><p>走进车间，眼前便是一个大数据工厂——量体采集数据下订单，订单传输到数据平台后，系统会自动完成版型匹配，并传输到生产部门。每一位工人都有一台电脑识别终端，所有流程的信息传递都在这上面进行。</p><br><p>“智能制造成为产业转型升级的关键领域。”中国互联网协会秘书长卢卫认为，制造业与互联网的加速融合，成为新一轮科技革命和产业变革的重大趋势。</p><br><p>中国互联网络信息中心发布第39次《中国互联网络发展状况统计报告》显示，超四成企业开展在线销售与采购，“互联网＋”传统产业融合加速。越来越多的企业认为，互联网会在经营制度、财务、生产制造等方面发挥越来越大的作用。</p><br><p>随时以举事，因资而立功，用万物之能而获利其上。</p><br><p>——党的十八届五中全会描绘了互联网发展的未来构想：实施网络强国战略，实施“互联网＋”行动计划，实施国家大数据战略。</p><br><p>——网络安全和信息化工作座谈会提出，“打通经济社会发展的信息‘大动脉’”。</p><br><p>——2017年2月，习近平总书记主持召开中央全面深化改革领导小组第三十二次会议，审议通过《关于推进公共信息资源开放的若干意见》，强调进一步强化信息资源深度整合，进一步促进信息惠民，进一步发挥数据大国、大市场优势，促进信息资源规模化创新应用，着力推进重点领域公共信息资源开放，释放经济价值和社会效应。</p><br><p>制造业、教育、文化、医疗、金融……各行各业的技术流、资金流、人才流、物资流随着信息流整合优化，全要素生产率得以提升，为推动创新发展、转变经济发展方式、调整经济结构发挥着积极作用。</p><br><p>网络信息带给传统经济的改变，不仅仅是经营和商业模式，更深刻的是思维模式：惟创新者进，惟创新者强，惟创新者胜。</p><br><p>当一夜之间，如雨后春笋般的共享单车涌入大街小巷；当“双十二”活动吸引越来越多境外国家和地区参与，北达北极圈，南到新西兰的萤火虫洞；当中国“芯”超级计算机首获世界冠军；当世界首颗量子科学实验卫星腾空而起……中国的网信事业在整个社会经济发展中发挥着越来越重要的作用，中国的信息技术也正从跟跑并跑向并跑领跑转变。</p><br><p>“互联网发展是中华民族的一个重要历史机遇，必须牢牢抓住，决不能同这样的历史机遇失之交臂。”阿里巴巴集团董事局主席马云说，为了在核心技术上取得突破，阿里巴巴在云计算、数据技术等领域已大规模投入，并致力于使互联网技术更普惠，让更多企业有这样的能力。“这是我们这一代人的历史责任”。</p><br><p>让亿万人民在共享互联网发展成果上有更多获得感</p><br><p>网信事业的发展为了谁？习近平总书记清晰地回答了这一根本性问题：“必须贯彻以人民为中心的发展思想”“让亿万人民在共享互联网发展成果上有更多获得感”。</p><br><p>今天的中国，网民规模达7．31亿，其中手机网民6．95亿，增长率连续三年超过10％。网信事业的发展与13亿多人民的工作和生活息息相关。</p><br><p>“十三五”期间，中国将以“创新、协调、绿色、开放、共享”的理念引领发展、推动变革。大力实施网络强国战略、国家大数据战略、“互联网＋”行动计划，促进互联网和经济社会融合发展，目的就是要让互联网给全体人民带来更多福祉。</p><br><p>——“发挥互联网在助推脱贫攻坚中的作用，推进精准扶贫、精准脱贫，让更多困难群众用上互联网。”</p><br><p>在湖北省襄阳市荆州街小学的多媒体教室里，年轻的英语老师站在摄像头前，给眼前屏幕上远在襄阳保康县油坊街小学的同学上课，全程用英语与对方教室里的10多个学生交流互动。这给深居秦巴山区的油坊街小学带来了实实在在的好处：贫困地区的孩子们也能享受优质教育资源。</p><br><p>2016年，网络扶贫行动计划深入实施，由中国互联网发展基金会、中国扶贫基金会联合企业共同发起成立了网络公益扶贫联盟，3．1万个贫困村实现了宽带建设和升级改造，158个国家级贫困县开展了电子商务进农村综合示范，“数字鸿沟”加快弥合。</p><br><p>——“要适应人民期待和需求，加快信息化服务普及，降低应用成本，为老百姓提供用得上、用得起、用得好的信息服务”。</p><br><p>据统计，截至2016年底，全国固定宽带平均接入速率达49Mbps；移动互联网户均接入流量是2014年12月的5倍多。2017年政府工作报告更是明确提出：年内全部取消手机国内长途和漫游费，大幅降低中小企业互联网专线接入资费，降低国际长途电话费。</p><br><p>——“百姓少跑腿、信息多跑路”。</p><br><p>“现在住院报销、助学贷款等很多事不用出村就能办，以前要花十天八天，现在一天就能办完。”宁夏银川市闽宁镇原隆村村民田成林为这样的高效率而感叹。宁夏回族自治区信息化建设办公室副主任文建国说，办件一旦受理就进入行政审批系统，所有流程在电子监察状态下运行，当事人也可全程跟踪查询，真正做到“百姓少跑腿、信息多跑路”。</p><br><p>2016年12月，国务院印发《“十三五”国家信息化规划》，提出着力满足广大人民群众普遍期待和经济社会发展关键需要，重点突破，推动信息技术更好服务经济升级和民生改善；着力深化改革，全面优化信息化发展环境，为如期全面建成小康社会提供强大动力。</p><br><p>——“网络空间天朗气清、生态良好，符合人民利益。”</p><br><p>为了打击违法犯罪行为，净化网络文化环境，2016年，相关部门相继组织开展“净网2016”专项行动、“清朗”系列专项行动，发布《互联网直播服务管理规定》重拳整治网络直播乱象等，网络生态进一步好转，网络空间日渐清朗。</p><br><p>“网络安全和信息化网信事业做得怎么样，不能仅用简单的技术指标来衡量了，而是要看民众从中获得了什么。”在复旦大学网络空间治理研究中心副主任沈逸看来，网络空间已经成为人类活动的第五疆域，习近平总书记从国家宏观战略的高度出发，深刻阐释了网信事业的本质特征，即网信事业的人民性。</p><br><p>没有网络安全就没有国家安全</p><br><p>互联网是社会发展的新引擎，也是国际竞争的新高地；信息技术为人们带来便利，也伴随着不少隐患：核心技术缺乏优势、网络诈骗大案时有发生、公民个人信息安全状况堪忧、全社会网络安全意识亟待提高……</p><br><p>“网络安全和信息化是相辅相成的。安全是发展的前提，发展是安全的保障，安全和发展要同步推进。”习近平总书记在多次讲话中反复强调网络安全的重要性。</p><br><p>早在2014年2月，习近平总书记就在中央网络安全和信息化领导小组第一次会议上，将网络安全的重要性提升到了前所未有的高度。“没有网络安全就没有国家安全”的论断，为之后一个时期的网络安全建设各项工作提出了根本遵循。在一年前的网络安全和信息化工作座谈会上，在中共中央政治局第三十六次集体学习时，习近平总书记多次对网络安全作出指示，以更为丰富和精辟的论述，为网络安全工作指明方向：</p><br><p>“树立正确的网络安全观。理念决定行动”；</p><br><p>“网络安全为人民，网络安全靠人民，维护网络安全是全社会共同责任”；</p><br><p>“加快提高网络管理水平，加快增强网络空间安全防御能力，加快用网络信息技术推进社会治理，加快提升我国对网络空间的国际话语权和规则制定权，朝着建设网络强国目标不懈努力”；</p><br><p>……</p><br><p>方向已定，号角吹响，各方正努力铸就“牢不可破”的网络安全防线。</p><br><p>——治网之道，法治为上。</p><br><p>2016年11月7日，我国网络空间安全领域的基础性法律——《中华人民共和国网络安全法》在十二届全国人大常委会第二十四次会议上高票获得通过，并将于2017年6月1日正式实施。</p><br><p>——兴网之道，标准为先。</p><br><p>2016年8月，中央网信办、国家质检总局、国家标准委联合印发《关于加强国家网络安全标准化工作的若干意见》，推动开展关键信息基础设施保护、网络安全审查、大数据安全、个人信息保护、新一代通信网络安全、互联网电视终端产品安全、网络安全信息共享等领域的标准研究和制定工作，统一权威的国家信息标准工作机制得以确立。</p><br><p>——强网之道，人才为重。</p><br><p>人才是网信安全的第一资源。2016年6月，中央网信办、发改委、教育部等6部门联合印发《关于加强网络安全学科建设和人才培养的意见》，推动开展网络安全学科专业和院系建设，创新网络安全人才培养机制等。同年9月，我国首次评选表彰“网络安全杰出人才”，中国工程院院士沈昌祥获此殊荣。</p><br><p>——安网之道，人民为本。</p><br><p>在2016年国家网络安全宣传周的活动中，广大公众近距离接触“全浸没式液冷服务器”等大批网络安全领域的“国之重器”，在互动体验中提升网络安全意识。“维护网络安全需要让每个人都成为参与者，不当旁观者，打击网络电信诈骗需要发动‘人民战争’。”360公司董事长周鸿祎深有感触。</p><br><p>面向2030，“网络空间安全”已被国家确立为电子信息领域的4个重大项目之一；“网络空间安全学院”在多所大学落地，第一批学生正在为建设网络强国、维护网络安全而日夜苦读；规划面积40平方公里的“国家网络安全人才与创新基地”已落户武汉，产学研一体化的“中国网络安全谷”不再停留于纸面……一个安全的国家网络空间，正在逐步铸就。</p><br><p>扩大国际“朋友圈”携手构建网络空间命运共同体</p><br><p>2017年3月1日，中国发布《网络空间国际合作战略》，用四项原则、六大目标、九大行动计划，向世界清晰描绘了中国面向全球网络空间的宏伟蓝图，为“构建网络空间命运共同体”提出中国主张。</p><br><p>“互联网让世界变成了地球村，推动国际社会越来越成为你中有我、我中有你的命运共同体。”在一年前的网络安全和信息化工作座谈会上，习近平总书记如是说。而早在2015年，他就提出，“网络空间是人类共同的活动空间，网络空间前途命运应由世界各国共同掌握。各国应该加强沟通、扩大共识、深化合作，共同构建网络空间命运共同体。”</p><br><p>网络空间给人类带来巨大机遇，同时也带来新的课题和挑战。规则不健全、秩序不合理等问题日益凸显；不同国家和地区间的信息鸿沟仍在拉大；国际网络空间治理规则难以平衡反映世界各国，特别是广大发展中国家的意愿和利益；世界范围内网络犯罪、网络攻击、网络恐怖主义活动等成为公害，影响着全球互联网均衡、安全、可持续发展。</p><br><p>面对新的机遇与挑战，国际社会应携起手来，共同维护网络空间和平、稳定与繁荣。近年来，中国以和平发展、合作共赢为主题，以构建网络空间命运共同体为目标，就推动网络空间国际交流合作提出中国主张，为破解全球网络治理难题贡献中国方案：</p><br><p>——推动国际交流。在第二届世界</p><p>互联网大会</p><p>上，习近平总书记提出了全球互联网发展治理的“四项原则”“五点主张”，得到国际社会积极响应。在第三届世界互联网大会上，他发表视频讲话，再次指出：“互联网发展是无国界、无边界的，利用好、发展好、治理好互联网必须深化网络空间国际合作，携手构建网络空间命运共同体。”</p><br><p>——深化国际合作。2016年中俄两国签署协作推进信息网络空间发展的联合声明；数字经济合作成为国际合作新亮点，2016年二十国集团杭州峰会制定了《二十国集团数字经济发展与合作倡议》；“一带一路”建设信息化发展进一步推进，统筹规划海底光缆和跨境陆地光缆建设，提高国际互联互通水平，打造网上丝绸之路。</p><br><p>——参与国际规则制定。2017年1月，中央网信办、国家标准委牵头建立了国家信息化领域标准化工作统筹推进机制，加快推动中国信息化标准走出去。此外，在移动通信、下一代互联网、下一代广播电视网、云计算、大数据、物联网、智能制造、智慧城市、网络安全等关键技术和重要领域，我国也积极参与国际标准制定。</p><br><p>“只有建立互联网新秩序，建立共同遵守的公约，才能保障互联网安全、健康、有序发展。”中国工程院院士、中国互联网协会理事长邬贺铨说，“构建网络空间命运共同体”的主张，反映了国际社会特别是广大发展中国家的共同心声，为推进全球互联网治理贡献了中国智慧。</p><br><p>网络的运转夜以继日，信息的传递须臾不停。</p><br><p>一年来，以习近平总书记重要讲话精神为指引，中国的网信工作不负历史和人民重托，交出了一份亮丽答卷；在信息革命新时代，中国必将以更自信、更有力、更坚定的步伐向网络强国目标奋勇迈进!</p><p><br></p>', '227', '12', '', '1', '1', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('5', '2', '1', '春天的8种营养果！再不吃就晚了', '', '柠檬由于味道较酸，一般不直接生吃，而是作为配菜或调味品。柠檬有“西餐之王”的美誉，凉菜、主菜、饮料中都能派上用场。', '', '<p>　　柠檬：天然美容品</p><p>　　柠檬由于味道较酸，一般不直接生吃，而是作为配菜或调味品。柠檬有“西餐之王”的美誉，凉菜、主菜、饮料中都能派上用场。</p><p>　　国家高级营养配餐师魏立军说，柠檬特殊的香气能祛除肉类和水产品的腥膻气味，并能使肉质更加细嫩。吃煎鳕鱼时将柠檬汁挤出来滴在鳕鱼上，巴掌大的鳕鱼滴4~5滴就可以。鳕鱼遇到柠檬汁，不但腥味没了，肉也更加鲜香;在烤好后的鱼、鸡翅上滴少许柠檬汁，味道也更香;还可以将柠檬的果肉切碎，和海鲜酱油一起调成汁儿，配着海鲜吃。</p><p></p><p>　　42柠檬富含维生素C，是种天然美容佳品，每天一杯柠檬水，能美白滋润皮肤。但应注意，柠檬味道酸，所以胃溃疡、胃酸分泌过多者应少吃柠檬。</p><p><br></p>', '72', '', '', '0', '0', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('6', '2', '1', '6大癌症谣言 你信了几条？', '', '患者的癌症是处于早期还是处于晚期？在治疗与康复上，二者有着很大区别。许多患者在被确诊为癌症晚期的那一刻，就果断放弃治疗了，认为治疗是毫无价值的。可事实上，多数晚期肿瘤患者是可以通过治疗提高生命质量的。在将来，随着医学的进步，晚期癌症治好的可能性也会不断提高。', 'upload/image/20170422/71863d3e0ff1754f582e248656d4c32d.jpg', '<p>　《多吃1食物 一辈子不得癌！》、《震惊国人！化疗不但能杀死癌细胞，还会杀死正常细胞！》……随着人们罹患癌症的医疗案例频频曝光，网络上到处都充斥着各种“癌症知识”，许多人为了抵抗癌症，也都乖乖地按照网上所说的去做。但实际上，网络上有许多“癌症知识”是危言耸听、缺乏科学依据的。下面，小编将为大家盘点一下关于癌症的6大谣言，朋友们可以对比一下自己，数数自己一共相信了多少条。</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6大癌症谣言 你信了几条？</p><p>　　6大癌症谣言 你信了几条？</p><p>　　谣言1：多吃抗癌食物就可以预防癌症</p><p>　　多吃大蒜可防癌，多吃蓝莓可抗癌……生活中有无数种食物被人们认定为防癌食物。虽说平时生活中多吃一些水果蔬菜对人体是有好处的，但把防癌抗癌的任务完全寄托在普通食材身上是没有多大意义的。因为我们的身体非常复杂，若单靠食物进行防癌抗癌，这种做法未免把癌症想得过于简单。</p><p>　　谣言2：化疗会杀死正常细胞，癌症患者应放弃化疗</p><p>　　许多癌症患者在接受化疗后，身体常会出现脱发、呕吐等副作用，而这，也使得人们对“化疗杀敌一千，自损八百”的说法深信不疑，也有许多患者会被这种言论主导想法，从而放弃化疗。其实，化疗是世界卫生组织推荐的常规治疗手段，作为目前癌症治疗的有效方法，化疗对于机体的不良影响远轻于肿瘤，加上如今药品技术的提高，化疗带来的副作用在不断减轻，因此癌症患者不必过于担心。</p><p>　　谣言3：癌症会传染，身边有癌症患者应警惕</p><p>　　癌症虽然很可怕，但还没可怕到会被感染的地步。有医学专家表示：从科学上来讲，目前人类癌细胞传染的概率相对来讲是非常小的，一个癌症患者若与一个健康的人接触，一般是不会传染癌症的。</p><p>　　谣言4、死于癌症治疗的人比死于癌症的人多</p><p>　　需要澄清的就是，癌症的治疗从来都不是一件容易的事。特别是到了癌症晚期，癌细胞会扩散至全身，治疗的话不太可能会完全“治愈”。在晚期治疗上，争取减缓患者的症状，延长患者的生存时间，才是治疗的重要之处。</p><p>　　谣言5、癌症到了晚期等于宣判死刑</p><p>　　患者的癌症是处于早期还是处于晚期？在治疗与康复上，二者有着很大区别。许多患者在被确诊为癌症晚期的那一刻，就果断放弃治疗了，认为治疗是毫无价值的。可事实上，多数晚期肿瘤患者是可以通过治疗提高生命质量的。在将来，随着医学的进步，晚期癌症治好的可能性也会不断提高。</p><p>　　谣言6、微波炉加热食物会致癌</p><p>　　虽说吃剩饭剩菜是一件令人感到不爽的事，但也不能把“致癌”的罪名强加给微波炉吧？微波炉加热食物仅仅是一种物理加热方法，这种加热方法对食物中营养物质的影响与其他加热方式无明显区别。如今也没有证据证明吃微波炉加热的食物会致癌的说法，所以大家不要自己吓自己。</p><p>　　以上是小编遴选的6条关于癌症的谣言，除了以上6条外，关于癌症的谣言依然有许多，如关于癌症的早期症状，就带有强烈的恐吓色彩：突然出现原因不明的消化不良、食欲迅速下降，注意这是胃癌的前兆；胸闷、气短、发热、痰中带血？你可能得了肺癌……网络上的文章，说的好像许多身体症状都有可能是癌前症状，在此小编建议大家擦亮双眼，不要对号入座，如若身体有任何不适，最好的解决办法就是提高警惕，及时前往医院确诊！　　看完上文，得出了一个道理就是：我们应该理性分辨网络信息的真假性，相信科学，避免虚假的健康资讯给自己或家人带来无谓的恐慌。</p><p><br></p>', '121', '', '', '0', '0', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('7', '3', '1', '男性乳腺发育异常：男性乳房变大是种病', '', '随着气温逐渐升高，衣服越穿越薄。对于一些男性而言，衣服渐薄就意味着“难言之隐”再也藏不住了。这里所说的“难言之隐”是指某些男性乳腺的异常发育，主要表现为胸部不断增大，甚至趋于乳房女性化，令人尴尬不已。', 'upload/image/20170422/ca934ada51b32bea28be37cfcc229f1b.jpg', '<p>        随着气温逐渐升高，衣服越穿越薄。对于一些男性而言，衣服渐薄就意味着“难言之隐”再也藏不住了。这里所说的“难言之隐”是指某些男性乳腺的异常发育，主要表现为胸部不断增大，甚至趋于乳房女性化，令人尴尬不已。</p><p>　　专家表示，男性乳腺发育异常近些年在临床上并不少见。近日就有媒体报道，湖南一名28岁男子就因为患上乳腺发育症而不自知，直至“纵容”乳房增大到C罩杯，衣服完全遮挡不住才去就医。</p><p>　　专家提醒，相比女性，男性对乳腺健康的意识和关注都远远不够。乳腺疾病并非女性所独有。工作生活压力、肥胖、不良生活方式会促使男性体内激素发生变化，引发乳腺疾病。</p><p>　　男性乳房增大</p><p>　　胖不是唯一原因</p><p>　　女性乳房是一个功能器官，是女性形体美最显著的一种标志。男性的乳腺虽然并不发育成熟，但这并不意味着男性就没有乳腺。某些男性因为各种原因乳房也会增大，甚至形状如女性，医学上称之为男性乳腺发育症。男性乳腺发育症又称男子女性型乳房，是男性乳房疾病中最为常见的一种。2016年，牛津英语词典新增的词条中就包括moobs（意为“男乳”）这个单词，特指异常突出的男性胸部。</p><p>　　肥胖被认为与男性乳腺发育异常呈正相关性。北京中医医院乳腺科主治医师谢芳告诉北京晨报记者，脂肪组织可以说是雌激素重要的来源，嗜食肥甘厚腻、暴饮暴食、久坐少动都是对男性乳腺健康非常不利的因素。这也正是为何男性肥胖者更多见乳腺过度发育的原因。</p><p>　　有些男性认为，乳房变大是因为自己太胖的缘故，只要去健身房有针对性的健身塑形就可以解决。谢芳表示，男性乳房发育还要看真性发育还是假性发育，“如果是假性的乳房发育，腺体并没有增加，只是脂肪组织的膨胀，局部为质地柔软的脂肪触感，乳房超声鉴别后也判定为假性，可以通过健身对乳房进行塑形。”</p><p>　　真性的乳房发育，小者仅乳晕皮下可摸到扁平圆形结节，大者近乎成年女性乳房。通常有触痛感，有时还会出现乳汁样分泌物。</p><p>　　国外对男性乳腺发育症发病率的报道为4%左右。虽然这种疾病在男性中并不多见，但一旦患了病，患者身心都会受到影响，感到窘迫，甚至精神压抑，渴望治疗。</p><p>　　乳腺发育尴尬可通过治疗化解</p><p>　　据介绍，男性乳腺异常发育有两个发病年龄高峰，分别为青春期及更年期，相对应称为青春期肥大和老年期肥大。</p><p>　　谢芳介绍，青春期前，男性与女性乳房发育类似，但是到了青春期，男性就只有导管结构发育，由于缺乏雌激素的刺激，不能够形成小叶结构。但是由于各种原因，导致激素的平衡失调，就会出现男性乳腺导管和间质组织的增大，出现腺体发育，甚至外观类似女性乳房大小。</p><p>　　老年患者可能与肾上腺和睾丸雄激素转化为雌激素过度有关，也可能与身体肥胖有关。</p><p>　　她补充道，中医将男性乳房腺体异常发育称为“乳疬”，认为由于男子肾气不充，肝失所养导致，放到今天这个大环境来说，生活节奏很快，工作生活压力往往导致男性承担很大压力，长期的情绪低落，焦虑往往无处宣泄，也是导致这个疾病其中的一个原因。肥胖也是另外一个因素。</p><p>　　对于单纯脂肪堆积导致的男性乳房肥大，减肥健身可以起到作用，甚至将软绵绵的脂肪变成硬邦邦的胸肌。但是对于伴有乳腺腺体发育的男性乳房发育患者，若想只通过减肥健身就达到消除隆起的乳房，是无法达到效果的。乳房腺体是受激素影响的，只有通过科学的治疗，才能到达消除乳房的目的。</p><p>　　治疗时，医生要先分析病因，排除其他疾病导致的乳腺发育，肝功是否异常以及用药史等，确定病因之后，再决定是否应该治疗原发疾病或者停用一些相关药物。如果是因为生活方式或者肥胖引起的，可以通过改善生活方式等方法进行纠正。如果伴有胀痛等症状，可口服一些药物来缓解，如果对外形要求较高，可以进行手术切除。</p><p>　　发现乳腺异常 定期自检及早干预</p><p>　　“男性乳房除了用来区分正反面其实没什么用。”虽然是句玩笑话，但也反映出大众对男性乳腺健康是完全忽视的，很多人甚至认为男性是根本不可能得乳腺癌的。</p><p>　　实际上，从理论上讲只要有乳腺组织，都有可能发生乳腺癌。尽管男性的乳房不发育，但是男性也有乳腺组织。男性体内也有雌激素，只是远远低于女性，这也是男性乳腺癌发生的最主要原因之一。</p><p>　　“有文献报道，男性乳腺癌发病率占乳腺癌的1%”，谢芳表示，由于男性乳腺癌不太常见以及男性对乳腺往往不太重视，因此，与女性相比，男性乳腺癌患者一般就诊时间相对较晚，很难早期发现。</p><p>　　医学界有一种观点认为，男性乳腺癌也具有遗传因素。专家建议，有乳腺癌家族史的男性最好留意自己乳房的变化，发现乳房肿块，应及时去专科医院检查，以免造成误诊。</p><p>　　男性乳腺癌比女性来得更为凶险。这是因为男性胸部平坦，乳房组织比女性少，就算只长了2-3厘米大的肿瘤，也可能在短时间内和周围的皮肤、肌肉、胸壁黏连、浸润。</p><p></p><p>　　谢芳提醒，男性同样应该对乳腺健康保持应有的重视，认识到男性也会出现乳腺疾病，经常进行乳房自检定期进行乳房检查，发现问题，及早进行干预。</p>', '183', '', '', '0', '0', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('8', '5', '1', '癌症离我们有多远？医生给出12条防癌建议！', '', '我国新增癌症患者数量在全球稳居第一位，而且可能在未来几十年都将处于世界第一位。以2012年统计数据为例，我国新增癌症患者307万人，全球新增癌症患者1400万人，占比超1/5。', '', '<p>　一、癌症离我们有多远</p><p>　　我国癌症新增发病率及死亡率占世界第一位</p><p>　　我国新增癌症患者数量在全球稳居第一位，而且可能在未来几十年都将处于世界第一位。以2012年统计数据为例，我国新增癌症患者307万人，全球新增癌症患者1400万人，占比超1/5。我国每年癌症死亡人数220万人，约占全球癌症死亡人数30%。如上图所示：到2050年，我国每年将有470多万人得癌症，还有370多万人将因癌症死去，癌症，离我们有多远？不远！在我国，每分钟有6人被诊断为癌症！</p><p>　　我国癌症分布：肺癌发病率及死亡率第一</p><p>　　我国高发癌症依次是肺癌、胃癌、结直肠癌、肝癌、食管癌，这与西方国家肺癌、乳腺癌、前列腺癌高发相比，差异比较大。随着生活水平的提高和生活习惯的变化，结直肠癌的发病也呈现比较明显的增长。</p><p>　　二、哪些因素会诱发癌症</p><p>　　遗传因素</p><p>　　有些肿瘤呈现家族性分布，有一定遗传性。</p><p>　　年龄增长</p><p>　　随着年龄增长，癌症发病风险增高。</p><p>　　环境污染</p><p>　　空气污染、水污染、土壤污染的加剧都会增加癌症风险。</p><p>　　吸烟</p><p>　　30%的癌症与吸烟有关，吸烟可以使肺癌、口腔癌、喉癌、气管癌、胰腺癌、胃癌、宫颈癌、膀胱癌发病率上升。二手烟的危害不容小觑。</p><p>　　酗酒</p><p>　　长期饮酒会导致肝脏病变，有诱发癌症危险。</p><p>　　放射性污染</p><p>　　医疗辐射、高空飞行、装修石材放射性污染等。</p><p>　　感染</p><p>　　有些感染会诱发癌症，例如乙肝病毒感染与肝癌、EB病毒感染与鼻咽癌；宫颈癌与HPV感染关联极大，可以通过接种疫苗、避免不洁性生活进行预防。胃癌与幽门螺旋杆菌的关联性很高，部分人群可以通过灭菌降低胃癌风险。分餐也是预防部分细菌、病毒感染的好的生活方式。</p><p>　　肥胖</p><p>　　肥胖能改变体内激素水平，增加食道癌、胰腺癌、结直肠癌、子宫癌、肾癌、乳腺癌6种癌症风险。小肠癌、胆囊癌、喉癌、膀胱癌、子宫颈癌、卵巢癌、脑癌、结缔组织肿瘤、淋巴癌等也与肥胖有一定关系。</p><p>　　三、如何降低癌症风险</p><p>　　三分之一癌症可以预防</p><p>　　世界卫生组织指出，大约有三分之一的癌症可以通过降低主要的风险因素进行预防：戒烟、健康饮食、锻炼身体、预防可造成癌症的感染。</p><p>　　防癌建议</p><p>　　世界癌症基金会和中国癌症基金会联合提出防癌建议12条：</p><p>　　1、戒烟限酒 这是防治所有慢性病的必要举措。</p><p>　　2、保持良好心态 这是提高免疫力的重要因素之一。</p><p>　　3、在正常体重范围内尽可能地瘦，不包括糖尿病患者。</p><p>　　4、每天至少进行30分钟身体活动&nbsp;走路是最好的运动，走路时要仰起头，挺起胸，嘴里唱着歌，手不断抓着，对提振精神、促进循环都有好处。</p><p>　　5、避免喝含糖饮料，限制摄入高热量食物（低纤维、高脂肪的加工食品）。</p><p>　　6、多吃各种蔬菜、水果、全麦和豆类。</p><p>　　7、限制红肉摄入，避免摄入加工的肉制品世界卫生组织提醒，一人一周摄入红肉量不超过500克。</p><p>　　8、如果喝酒，男性每天不超过两份，女性不超过一份。（一份酒精含量为10~15克）</p><p>　　9、限制盐腌食品或用盐加工的食品 腌制品中有亚硝酸盐，熏制品也应该少吃。</p><p>　　10、不用膳食补充剂预防癌症 少吃保健品。</p><p>　　11、母亲对婴儿进行6个月的全母乳喂养，可减少乳腺癌的发生。</p><p><br/></p><p>　　12、癌症患者治疗后应遵循癌症预防建议。</p><p><br/></p>', '245', '', '', '1', '1', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('9', '5', '1', '3123', '科技,互联网,奶罩', '二', '', '<p>二</p>', '184', '', '', '0', '0', '1', '2017', '2017');
INSERT INTO `edms_article` VALUES ('10', '4', '1', '大城市学位紧张，是外地人太多了吗？', '女大学生', '邾城街是武汉新洲区城关所在，1905年建校的邾城街第一小学，是新洲区唯一一所百年老校。随着大量农村人口涌进城镇，这所小学被4331名学生挤成了武汉市在校生人数最多的小学，大大超过学校承受的3000人极限。虽然学校语数外三科成绩在全区小学经常名列第一，但师生们还是被“挤”的难受。', '', '<p>邾城街是武汉新洲区城关所在，1905年建校的邾城街第一小学，是新洲区唯一一所百年老校。随着大量农村人口涌进城镇，这所小学被4331名学生挤成了武汉市在校生人数最多的小学，大大超过学校承受的3000人极限。虽然学校语数外三科成绩在全区小学经常名列第一，但师生们还是被“挤”的难受。现场：每天有保安站“厕所安全岗”</p><p style=\"text-align: center;\"></p><p>4月23日，记者体验了邾城一小的升旗仪式。因为人数多、场地窄，学生并不能随意进出操场，学校为他们制定了严格的次序和流程。4331名学生、70个班级分布在3栋各有4层的教学楼里，8点一到，70个班的学生走出教室列队，挤满每一条走廊。记者跟随4楼的五(2)班站一起，他们班在最高的楼层4楼、 又离楼梯最近，所以第一个出发。4楼的3个班全部走出楼梯，后面依次才轮到三楼、二楼的班级下楼，最后是一楼的班级。等70个班全部在操场指定位置站定，用时10分钟。学校一高一低两个操场，由一条绿化带隔开，有3个班被指定站在绿化带上。此时，望向被4331名学生站满后的操场，体会了什么叫密密麻麻。<br/></p><p><br/></p><p>“学校承载极限是3000名学生。” 校长张伟告诉记者，学校超载引发系列管理难题，最平常不过的校园生活如做操、放学、上厕所皆成安全风险点，学校压力非常大，为此想尽各种办法。</p><p><br/>8：55第一节课正式开课前，很多学生上厕所。两栋教学楼之间的一楼厕所外，记者看到保安石德英正在站“厕所安全岗”。这处男女各10个蹲位的厕所被指定给14个班使用，此时男厕所门前孩子们有点小堵，石德英立刻走过去吩咐孩子们站队并留出通道。石德英告诉记者，每个课间和集体活动时间，这个点位都安排有一名保安站“厕所安全岗”，一旦厕所门口出现人多不流通，保安就得上去防拥挤、摔倒和踩伤。全校共有7处厕所，每个课间为了让4300多名学生短时间内安全上厕所，该校有班级学生上厕所安排表，哪个班级具体到哪里上厕所都做了详细分配。</p><p><br/></p><p>9：40—10：10，30分钟的大课间时间，因为操场不够用，每天只能容纳两个年级的学生进行户外体育活动。周一是一年级和四年级，记者看到一年级学生集中在一个操场跳绳、玩耍等，四年级学生集中在另一个操场上各自打篮球、跳长绳、甩呼啦圈，其他年级的孩子们则在教室读国学经典或其他安静的自由活动。</p><p><br/></p><p>中午放学时间到，不在校就餐的2700多名学生，分别在11：30 、11：40两个时段错峰放学。到了下午，4331名学生的放学时间分成4：05、4：15和4：45三个时段，尽管如此，3000名家 长陆续涌进学校接孩子，还是把学校和校门前一条街堵成了粥。</p>', '403', '', '', '1', '1', '1', '2017', '2017');

-- ----------------------------
-- Table structure for edms_article_comment
-- ----------------------------
DROP TABLE IF EXISTS `edms_article_comment`;
CREATE TABLE `edms_article_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论ID',
  `article_id` int(11) NOT NULL COMMENT '评论文章ID',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父ID，回复评论ID',
  `to_uid` int(11) NOT NULL COMMENT '被回复者UID',
  `from_uid` int(11) NOT NULL COMMENT '回复者UID',
  `content` text NOT NULL COMMENT '评论内容',
  `like` int(11) NOT NULL DEFAULT '0' COMMENT '喜欢数量',
  `dislike` int(11) NOT NULL DEFAULT '0' COMMENT '不喜欢数量',
  `status` tinyint(4) DEFAULT '0' COMMENT '状态 0待审核 1已经审核',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='文章评论';

-- ----------------------------
-- Records of edms_article_comment
-- ----------------------------
INSERT INTO `edms_article_comment` VALUES ('3', '1', '0', '1', '2', '2311322', '1', '1', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('4', '2', '0', '1', '2', '电话是多少', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('5', '1', '0', '1', '2', '这是一个评论哈啊', '2', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('6', '1', '0', '0', '1', '123123', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('7', '1', '0', '0', '1', '将带动5千亿治水市场', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('8', '8', '0', '0', '1', '根据统计数据发现，在我国约九成的肝癌患者都曾经感染过乙肝病毒。乙肝感染很容易引起肝硬化，也容易对肝脏正常细胞造成伤害，从而更容易令正常的肝脏细胞转化为肿瘤细胞。因此，如果曾经感染过肝炎病毒的人，都要更关注自己的肝脏情况。', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('9', '1', '0', '0', '2', 'fsdfdsfs', '9', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('10', '1', '9', '2', '1', '回复一条评论内容', '3', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('11', '1', '9', '2', '1', '短发身份', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('12', '10', '0', '0', '1', '来发个评论试试 ~', '4', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('13', '10', '12', '1', '1', '哈哈', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('14', '10', '12', '1', '1', '呵呵', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('15', '4', '0', '0', '1', '123\r\n', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('16', '10', '0', '0', '2', '哈哈', '1', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('17', '10', '16', '2', '2', '我', '0', '0', '1', '0');
INSERT INTO `edms_article_comment` VALUES ('18', '10', '16', '2', '2', '我', '0', '0', '1', '0');

-- ----------------------------
-- Table structure for edms_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `edms_article_tag`;
CREATE TABLE `edms_article_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'tag标签ID',
  `title` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '标签名',
  `keywords` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'seo_关键词',
  `description` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'seo_栏目描述',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT 'TAG引用数量',
  `create_at` char(10) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `text_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tags标签';

-- ----------------------------
-- Records of edms_article_tag
-- ----------------------------
INSERT INTO `edms_article_tag` VALUES ('1', '科技', '', '', '0', '0');
INSERT INTO `edms_article_tag` VALUES ('2', '互联网', '', '', '1', '0');
INSERT INTO `edms_article_tag` VALUES ('3', '奶罩', '', '', '1', '0');
INSERT INTO `edms_article_tag` VALUES ('4', '习近平', '', '', '1', '0');
INSERT INTO `edms_article_tag` VALUES ('5', '女大学生', '', '', '1', '0');

-- ----------------------------
-- Table structure for edms_article_tag_access
-- ----------------------------
DROP TABLE IF EXISTS `edms_article_tag_access`;
CREATE TABLE `edms_article_tag_access` (
  `article_id` int(11) NOT NULL COMMENT '关联文章ID',
  `tag_id` int(11) NOT NULL COMMENT '关联TagID',
  KEY `article_id` (`article_id`) USING BTREE,
  KEY `tag_id` (`tag_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_article_tag_access
-- ----------------------------
INSERT INTO `edms_article_tag_access` VALUES ('3', '1');
INSERT INTO `edms_article_tag_access` VALUES ('9', '1');
INSERT INTO `edms_article_tag_access` VALUES ('9', '2');
INSERT INTO `edms_article_tag_access` VALUES ('9', '3');
INSERT INTO `edms_article_tag_access` VALUES ('1', '4');
INSERT INTO `edms_article_tag_access` VALUES ('10', '5');

-- ----------------------------
-- Table structure for edms_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `edms_auth_access`;
CREATE TABLE `edms_auth_access` (
  `group_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_auth_access
-- ----------------------------
INSERT INTO `edms_auth_access` VALUES ('1', '75');
INSERT INTO `edms_auth_access` VALUES ('1', '1');
INSERT INTO `edms_auth_access` VALUES ('1', '2');
INSERT INTO `edms_auth_access` VALUES ('1', '3');
INSERT INTO `edms_auth_access` VALUES ('1', '4');
INSERT INTO `edms_auth_access` VALUES ('1', '5');
INSERT INTO `edms_auth_access` VALUES ('1', '29');
INSERT INTO `edms_auth_access` VALUES ('1', '30');
INSERT INTO `edms_auth_access` VALUES ('1', '83');
INSERT INTO `edms_auth_access` VALUES ('1', '6');
INSERT INTO `edms_auth_access` VALUES ('1', '7');
INSERT INTO `edms_auth_access` VALUES ('1', '8');
INSERT INTO `edms_auth_access` VALUES ('1', '9');
INSERT INTO `edms_auth_access` VALUES ('1', '10');
INSERT INTO `edms_auth_access` VALUES ('1', '31');
INSERT INTO `edms_auth_access` VALUES ('1', '32');
INSERT INTO `edms_auth_access` VALUES ('1', '52');
INSERT INTO `edms_auth_access` VALUES ('1', '53');
INSERT INTO `edms_auth_access` VALUES ('1', '54');
INSERT INTO `edms_auth_access` VALUES ('1', '76');
INSERT INTO `edms_auth_access` VALUES ('1', '56');
INSERT INTO `edms_auth_access` VALUES ('1', '57');
INSERT INTO `edms_auth_access` VALUES ('1', '58');
INSERT INTO `edms_auth_access` VALUES ('1', '59');
INSERT INTO `edms_auth_access` VALUES ('1', '60');
INSERT INTO `edms_auth_access` VALUES ('1', '61');
INSERT INTO `edms_auth_access` VALUES ('1', '62');
INSERT INTO `edms_auth_access` VALUES ('1', '63');
INSERT INTO `edms_auth_access` VALUES ('1', '64');
INSERT INTO `edms_auth_access` VALUES ('1', '65');
INSERT INTO `edms_auth_access` VALUES ('1', '66');
INSERT INTO `edms_auth_access` VALUES ('1', '67');
INSERT INTO `edms_auth_access` VALUES ('1', '68');
INSERT INTO `edms_auth_access` VALUES ('1', '69');
INSERT INTO `edms_auth_access` VALUES ('1', '70');
INSERT INTO `edms_auth_access` VALUES ('1', '71');
INSERT INTO `edms_auth_access` VALUES ('1', '72');
INSERT INTO `edms_auth_access` VALUES ('1', '73');
INSERT INTO `edms_auth_access` VALUES ('1', '74');
INSERT INTO `edms_auth_access` VALUES ('1', '11');
INSERT INTO `edms_auth_access` VALUES ('1', '12');
INSERT INTO `edms_auth_access` VALUES ('1', '13');
INSERT INTO `edms_auth_access` VALUES ('1', '14');
INSERT INTO `edms_auth_access` VALUES ('1', '15');
INSERT INTO `edms_auth_access` VALUES ('1', '33');
INSERT INTO `edms_auth_access` VALUES ('1', '34');
INSERT INTO `edms_auth_access` VALUES ('1', '16');
INSERT INTO `edms_auth_access` VALUES ('1', '17');
INSERT INTO `edms_auth_access` VALUES ('1', '18');
INSERT INTO `edms_auth_access` VALUES ('1', '19');
INSERT INTO `edms_auth_access` VALUES ('1', '35');
INSERT INTO `edms_auth_access` VALUES ('1', '36');
INSERT INTO `edms_auth_access` VALUES ('1', '39');
INSERT INTO `edms_auth_access` VALUES ('1', '40');
INSERT INTO `edms_auth_access` VALUES ('1', '41');
INSERT INTO `edms_auth_access` VALUES ('1', '42');
INSERT INTO `edms_auth_access` VALUES ('1', '43');
INSERT INTO `edms_auth_access` VALUES ('1', '44');
INSERT INTO `edms_auth_access` VALUES ('1', '45');
INSERT INTO `edms_auth_access` VALUES ('1', '46');
INSERT INTO `edms_auth_access` VALUES ('1', '47');
INSERT INTO `edms_auth_access` VALUES ('1', '48');
INSERT INTO `edms_auth_access` VALUES ('1', '49');
INSERT INTO `edms_auth_access` VALUES ('1', '50');
INSERT INTO `edms_auth_access` VALUES ('1', '51');
INSERT INTO `edms_auth_access` VALUES ('1', '20');
INSERT INTO `edms_auth_access` VALUES ('1', '21');
INSERT INTO `edms_auth_access` VALUES ('1', '22');
INSERT INTO `edms_auth_access` VALUES ('1', '23');
INSERT INTO `edms_auth_access` VALUES ('1', '24');
INSERT INTO `edms_auth_access` VALUES ('1', '37');
INSERT INTO `edms_auth_access` VALUES ('1', '38');
INSERT INTO `edms_auth_access` VALUES ('1', '25');
INSERT INTO `edms_auth_access` VALUES ('1', '26');
INSERT INTO `edms_auth_access` VALUES ('1', '27');
INSERT INTO `edms_auth_access` VALUES ('1', '28');
INSERT INTO `edms_auth_access` VALUES ('1', '55');
INSERT INTO `edms_auth_access` VALUES ('1', '81');
INSERT INTO `edms_auth_access` VALUES ('1', '77');
INSERT INTO `edms_auth_access` VALUES ('1', '78');
INSERT INTO `edms_auth_access` VALUES ('1', '79');
INSERT INTO `edms_auth_access` VALUES ('1', '80');
INSERT INTO `edms_auth_access` VALUES ('1', '85');
INSERT INTO `edms_auth_access` VALUES ('1', '86');
INSERT INTO `edms_auth_access` VALUES ('1', '87');
INSERT INTO `edms_auth_access` VALUES ('1', '88');
INSERT INTO `edms_auth_access` VALUES ('1', '89');
INSERT INTO `edms_auth_access` VALUES ('1', '90');
INSERT INTO `edms_auth_access` VALUES ('1', '91');
INSERT INTO `edms_auth_access` VALUES ('2', '75');
INSERT INTO `edms_auth_access` VALUES ('2', '1');
INSERT INTO `edms_auth_access` VALUES ('2', '2');
INSERT INTO `edms_auth_access` VALUES ('2', '3');
INSERT INTO `edms_auth_access` VALUES ('2', '4');
INSERT INTO `edms_auth_access` VALUES ('2', '29');
INSERT INTO `edms_auth_access` VALUES ('2', '30');
INSERT INTO `edms_auth_access` VALUES ('2', '83');
INSERT INTO `edms_auth_access` VALUES ('2', '6');
INSERT INTO `edms_auth_access` VALUES ('2', '7');
INSERT INTO `edms_auth_access` VALUES ('2', '8');
INSERT INTO `edms_auth_access` VALUES ('2', '9');
INSERT INTO `edms_auth_access` VALUES ('2', '10');
INSERT INTO `edms_auth_access` VALUES ('2', '31');
INSERT INTO `edms_auth_access` VALUES ('2', '32');
INSERT INTO `edms_auth_access` VALUES ('2', '52');
INSERT INTO `edms_auth_access` VALUES ('2', '53');
INSERT INTO `edms_auth_access` VALUES ('2', '54');
INSERT INTO `edms_auth_access` VALUES ('2', '76');
INSERT INTO `edms_auth_access` VALUES ('2', '56');
INSERT INTO `edms_auth_access` VALUES ('2', '57');
INSERT INTO `edms_auth_access` VALUES ('2', '58');
INSERT INTO `edms_auth_access` VALUES ('2', '59');
INSERT INTO `edms_auth_access` VALUES ('2', '61');
INSERT INTO `edms_auth_access` VALUES ('2', '62');
INSERT INTO `edms_auth_access` VALUES ('2', '63');
INSERT INTO `edms_auth_access` VALUES ('2', '64');
INSERT INTO `edms_auth_access` VALUES ('2', '65');
INSERT INTO `edms_auth_access` VALUES ('2', '66');
INSERT INTO `edms_auth_access` VALUES ('2', '68');
INSERT INTO `edms_auth_access` VALUES ('2', '69');
INSERT INTO `edms_auth_access` VALUES ('2', '70');
INSERT INTO `edms_auth_access` VALUES ('2', '71');
INSERT INTO `edms_auth_access` VALUES ('2', '73');
INSERT INTO `edms_auth_access` VALUES ('2', '74');
INSERT INTO `edms_auth_access` VALUES ('2', '11');
INSERT INTO `edms_auth_access` VALUES ('2', '12');
INSERT INTO `edms_auth_access` VALUES ('2', '13');
INSERT INTO `edms_auth_access` VALUES ('2', '14');
INSERT INTO `edms_auth_access` VALUES ('2', '16');
INSERT INTO `edms_auth_access` VALUES ('2', '17');
INSERT INTO `edms_auth_access` VALUES ('2', '18');
INSERT INTO `edms_auth_access` VALUES ('2', '39');
INSERT INTO `edms_auth_access` VALUES ('2', '40');
INSERT INTO `edms_auth_access` VALUES ('2', '41');
INSERT INTO `edms_auth_access` VALUES ('2', '43');
INSERT INTO `edms_auth_access` VALUES ('2', '46');
INSERT INTO `edms_auth_access` VALUES ('2', '47');
INSERT INTO `edms_auth_access` VALUES ('2', '48');
INSERT INTO `edms_auth_access` VALUES ('2', '20');
INSERT INTO `edms_auth_access` VALUES ('2', '21');
INSERT INTO `edms_auth_access` VALUES ('2', '22');
INSERT INTO `edms_auth_access` VALUES ('2', '23');
INSERT INTO `edms_auth_access` VALUES ('2', '25');
INSERT INTO `edms_auth_access` VALUES ('2', '26');
INSERT INTO `edms_auth_access` VALUES ('2', '27');
INSERT INTO `edms_auth_access` VALUES ('2', '28');
INSERT INTO `edms_auth_access` VALUES ('2', '55');
INSERT INTO `edms_auth_access` VALUES ('2', '81');
INSERT INTO `edms_auth_access` VALUES ('2', '77');
INSERT INTO `edms_auth_access` VALUES ('2', '79');
INSERT INTO `edms_auth_access` VALUES ('2', '80');
INSERT INTO `edms_auth_access` VALUES ('2', '85');
INSERT INTO `edms_auth_access` VALUES ('2', '86');
INSERT INTO `edms_auth_access` VALUES ('2', '88');

-- ----------------------------
-- Table structure for edms_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `edms_auth_group`;
CREATE TABLE `edms_auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '用户组名称',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户组';

-- ----------------------------
-- Records of edms_auth_group
-- ----------------------------
INSERT INTO `edms_auth_group` VALUES ('1', '超级管理员', '管理员');
INSERT INTO `edms_auth_group` VALUES ('2', '后台演示用户', '请勿删除演示内容');

-- ----------------------------
-- Table structure for edms_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `edms_auth_rule`;
CREATE TABLE `edms_auth_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '规则名称',
  `module` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `action` varchar(64) NOT NULL,
  `is_menu` enum('0','1') NOT NULL DEFAULT '0' COMMENT '是否为菜单0 否 1是',
  `icon` varchar(64) NOT NULL DEFAULT 'fa fa-circle-o' COMMENT '图标',
  `reorder` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COMMENT='权限规则表';

-- ----------------------------
-- Records of edms_auth_rule
-- ----------------------------
INSERT INTO `edms_auth_rule` VALUES ('1', '0', '内容', '#', '#', '#', '1', 'fa fa-database', '2', '');
INSERT INTO `edms_auth_rule` VALUES ('2', '1', '栏目管理', 'admin', 'category', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('3', '2', '创建栏目页', 'admin', 'category', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('4', '2', '栏目更新页', 'admin', 'category', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('5', '2', '栏目删除', 'admin', 'category', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('6', '1', '文章管理', 'admin', 'article', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('7', '6', '创建文章页', 'admin', 'article', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('8', '6', '文章更新页', 'admin', 'article', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('9', '6', '文章删除', 'admin', 'article', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('10', '6', '文章推送操作', 'admin', 'article', 'handle', '0', 'fa fa-circle-o', '0', '文章推送等操作');
INSERT INTO `edms_auth_rule` VALUES ('11', '0', '用户', '#', '#', '#', '1', 'fa fa-user', '3', '');
INSERT INTO `edms_auth_rule` VALUES ('12', '11', '用户列表 ', 'admin', 'user', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('13', '12', '创建成员页', 'admin', 'user', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('14', '12', '成员更新页', 'admin', 'user', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('15', '12', '成员删除', 'admin', 'user', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('16', '11', '用户组', 'admin', 'group', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('17', '16', '创建组页', 'admin', 'group', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('18', '16', '组更新页', 'admin', 'group', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('19', '16', '组删除', 'admin', 'group', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('20', '0', '系统', '#', '#', '#', '1', 'fa fa-gear', '5', '');
INSERT INTO `edms_auth_rule` VALUES ('21', '20', '配置项', 'admin', 'config', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('22', '21', '创建配置页', 'admin', 'config', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('23', '21', '配置更新页', 'admin', 'config', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('24', '21', '配置删除', 'admin', 'config', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('25', '20', '网站配置', '#', '#', '#', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('26', '25', '站点信息', 'admin', 'system', 'site', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('27', '25', '验证码设置', 'admin', 'system', 'captcha', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('28', '25', '水印设置', 'admin', 'system', 'water', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('29', '2', '创建栏目操作', 'admin', 'category', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('30', '2', '栏目更新操作', 'admin', 'category', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('31', '6', '创建文章操作', 'admin', 'article', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('32', '6', '文章更新操作', 'admin', 'article', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('33', '12', '创建成员操作', 'admin', 'user', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('34', '12', '成员更新操作', 'admin', 'user', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('35', '16', '创建组操作', 'admin', 'group', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('36', '16', '组更新操作', 'admin', 'group', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('37', '21', '创建配置操作', 'admin', 'config', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('38', '21', '配置更新操作', 'admin', 'config', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('39', '0', '运营', '#', '#', '#', '1', 'fa fa-cube', '4', '');
INSERT INTO `edms_auth_rule` VALUES ('40', '39', '友情链接', 'admin', 'link', 'index', '1', 'fa fa-link', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('41', '40', '创建友情链接页', 'admin', 'link', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('42', '40', '创建友情链接操作', 'admin', 'link', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('43', '40', '友情链接更新页', 'admin', 'link', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('44', '40', '友情链接更新操作', 'admin', 'link', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('45', '40', '友情链接删除', 'admin', 'link', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('46', '39', '幻灯片', 'admin', 'slider', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('47', '46', '创建幻灯片页', 'admin', 'slider', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('48', '46', '幻灯片更新页', 'admin', 'slider', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('49', '46', '幻灯片删除', 'admin', 'slider', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('50', '46', '创建幻灯片操作', 'admin', 'slider', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('51', '46', '幻灯片更新操作', 'admin', 'slider', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('52', '1', '评论管理', 'admin', 'comment', 'index', '1', 'fa fa-comments-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('53', '52', '评论删除', 'admin', 'comment', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('54', '52', '评论操作', 'admin', 'comment', 'handle', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('55', '25', '缩略图设置', 'admin', 'system', 'thumb', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('56', '1', '书籍管理', 'admin', 'book', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('57', '56', '创建书籍', 'admin', 'book', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('58', '56', '书籍更新页', 'admin', 'book', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('59', '56', '书籍操作', 'admin', 'book', 'handle', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('60', '56', '书籍删除', 'admin', 'book', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('61', '56', '书籍创建操作', 'admin', 'book', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('62', '56', '书籍更新操作', 'admin', 'book', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('63', '56', '创建分卷', 'admin', 'section', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('64', '56', '分卷更新页', 'admin', 'section', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('65', '56', '分卷创建操作', 'admin', 'section', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('66', '56', '分卷更新操作', 'admin', 'section', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('67', '56', '分卷删除', 'admin', 'section', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('68', '56', '创建章节', 'admin', 'chapter', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('69', '56', '章节更新页', 'admin', 'chapter', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('70', '56', '章节创建操作', 'admin', 'chapter', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('71', '56', '章节更新操作', 'admin', 'chapter', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('72', '56', '章节删除', 'admin', 'chapter', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('73', '56', '分卷', 'admin', 'section', 'index', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('74', '56', '章节', 'admin', 'chapter', 'index', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('75', '0', '首页', 'admin', 'index', 'index', '1', 'fa fa-home', '1', '');
INSERT INTO `edms_auth_rule` VALUES ('76', '52', '评论审核', 'admin', 'comment', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('77', '20', '主题设置', 'admin', 'theme', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('78', '77', '主题切换', 'admin', 'theme', 'change', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('79', '20', '更新缓存', 'admin', 'clean', 'index', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('80', '79', '更新缩略图', 'admin', 'clean', 'thumb', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('81', '25', '邮件设置', 'admin', 'system', 'email', '1', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('82', '25', '2', '', '', '', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('83', '2', '栏目排序', 'admin', 'category', 'reorder', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('84', '25', '1', '#', '#', '', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('85', '20', '路由规则', 'admin', 'rule', 'index', '1', 'fa fa-circle-o', '3', '');
INSERT INTO `edms_auth_rule` VALUES ('86', '85', '规则添加', 'admin', 'rule', 'create', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('87', '85', '规则添加操作', 'admin', 'rule', 'store', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('88', '85', '规则编辑', 'admin', 'rule', 'edit', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('89', '85', '规则编辑操作', 'admin', 'rule', 'update', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('90', '85', '规则删除', 'admin', 'rule', 'remove', '0', 'fa fa-circle-o', '0', '');
INSERT INTO `edms_auth_rule` VALUES ('91', '85', '规则排序', 'admin', 'rule', 'reorder', '0', 'fa fa-circle-o', '0', '');

-- ----------------------------
-- Table structure for edms_book
-- ----------------------------
DROP TABLE IF EXISTS `edms_book`;
CREATE TABLE `edms_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图书ID',
  `uid` int(11) NOT NULL COMMENT '发布者UID',
  `category_id` int(11) NOT NULL COMMENT '分类ID',
  `title` varchar(64) NOT NULL COMMENT '图书名称',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '封面图片路径',
  `keywords` varchar(200) NOT NULL DEFAULT '' COMMENT '作者名称',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述简介',
  `content` text NOT NULL COMMENT '图书内容',
  `recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐 0默认 1推荐 ',
  `hot` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否热门 0默认  1热门',
  `author` varchar(16) NOT NULL DEFAULT '' COMMENT '图书作者',
  `publishing` varchar(64) NOT NULL DEFAULT '' COMMENT '出版社',
  `date` int(10) DEFAULT '0' COMMENT '出版时间',
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `isbn` varchar(64) NOT NULL DEFAULT '' COMMENT '书号ISBN',
  `is_section` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否分卷 0不分卷 1分卷',
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0连载 1完结 ',
  `pv` int(11) NOT NULL DEFAULT '0' COMMENT 'PV',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 待审核 1 审核',
  `update_at` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='图书';

-- ----------------------------
-- Records of edms_book
-- ----------------------------
INSERT INTO `edms_book` VALUES ('1', '1', '7', '史记', 'upload/image/20170504/3ba60c359946da900ced95a0dfadf911.jpeg', '', '《史记》是由司马迁撰写的中国第一部纪传体通史。记载了上自上古传说中的黄帝时代，下至汉武帝元狩元年间共3000多年的历史（哲学、政治、经济、军事等）。《史记》最初没有固定书名，或称“太史公书”，或称“太史公传”，也省称“太史公”。“史记”本是古代史书通称，从三国时期开始，“史记”由史书的通称逐渐成为“太史公书”的专称。《史记》与后来的《汉书》（班固）、《后汉书》（范晔、司马彪）、《三国志》（陈寿）合称“前四史”。刘向等人认为此书“善序事理，辩而不华，质而不俚”。与司马光的《资治通鉴》并称“史学双璧”。', '《史记》是由司马迁撰写的中国第一部纪传体通史。记载了上自上古传说中的黄帝时代，下至汉武帝元狩元年间共3000多年的历史（哲学、政治、经济、军事等）。《史记》最初没有固定书名，或称“太史公书”，或称“太史公传”，也省称“太史公”。“史记”本是古代史书通称，从三国时期开始，“史记”由史书的通称逐渐成为“太史公书”的专称。《史记》与后来的《汉书》（班固）、《后汉书》（范晔、司马彪）、《三国志》（陈寿）合称“前四史”。刘向等人认为此书“善序事理，辩而不华，质而不俚”。与司马光的《资治通鉴》并称“史学双璧”。', '0', '1', '司马迁', '人民出版社', '2147483647', '0.00', '85214441', '1', '1', '0', '1', '2017', '0');
INSERT INTO `edms_book` VALUES ('2', '1', '7', '2134', '', '1', '2', '<p>3</p><p>2</p>', '1', '0', '4', '5', '2147483647', '0.00', '23', '1', '1', '0', '1', '2017', '0');

-- ----------------------------
-- Table structure for edms_book_chapter
-- ----------------------------
DROP TABLE IF EXISTS `edms_book_chapter`;
CREATE TABLE `edms_book_chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '章节ID',
  `book_id` int(11) NOT NULL COMMENT '图书ID',
  `section_id` int(11) NOT NULL DEFAULT '0' COMMENT '卷ID',
  `title` varchar(200) NOT NULL COMMENT '章节标题',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '章节关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '章节描述',
  `content` text NOT NULL COMMENT '章节内容',
  `create_at` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_at` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_book_chapter
-- ----------------------------
INSERT INTO `edms_book_chapter` VALUES ('3', '1', '5', '三代世表', '32', '太史公曰：五帝、三代之记，尚矣。自殷以前诸侯不可得而谱，周以来乃颇可著。孔子因史文次春秋，纪元年，正时日月，盖其详哉。至於序尚书则略，无年月；或颇有，然多阙，不可录。故疑则传疑，盖其慎也。', '<p>　　太史公曰：五帝、三代之记，尚矣。自殷以前诸侯不可得而谱，周以来乃颇可著。孔子因史文次春秋，纪元年，正时日月，盖其详哉。至於序尚书则略，无年月；或颇有，然多阙，不可录。故疑则传疑，盖其慎也。</p><p>　　余读谍记，黄帝以来皆有年数。稽其历谱谍终始五德之传，古文咸不同，乖异。夫子之弗论次其年月，岂虚哉！於是以五帝系谍、尚书集世纪黄帝以来讫共和为世表。</p><p>　　（表略）</p><p>　　张夫子问褚先生曰：“诗言契、后稷皆无父而生。今案诸传记咸言有父，父皆黄帝子也，得无与诗谬秋？”</p><p>　　褚先生曰：“不然。诗言契生於卵，后稷人迹者，欲见其有天命精诚之意耳。鬼神不能自成，须人而生，柰何无父而生乎！一言有父，一言无父，信以传信，疑以传疑，故两言之。尧知契、稷皆贤人，天之所生，故封之契七十里，後十馀世至汤，王天下。尧知后稷子孙之後王也，故益封之百里，其後世且千岁，至文王而有天下。诗传曰：“汤之先为契，无父而生。契母与姊妹浴於玄丘水，有燕衔卵堕之，契母得，故含之，误吞之，即生契。契生而贤，尧立为司徒，姓之曰子氏。子者兹；兹，益大也。诗人美而颂之曰“殷社芒芒，天命玄鸟，降而生商”。商者质，殷号也。文王之先为后稷，后稷亦无父而生。后稷母为姜嫄，出见大人迹而履践之，知於身，则生后稷。姜嫄以为无父，贱而弃之道中，牛羊避不践也。抱之山中，山者养之。又捐之大泽，鸟覆席食之。姜嫄怪之，於是知其天子，乃取长之。尧知其贤才，立以为大农，姓之曰姬氏。姬者，本也。诗人美而颂之曰“厥初生民”，深修益成，而道后稷之始也。”孔子曰：“昔者尧命契为子氏，为有汤也。命后稷为姬氏，为有文王也。大王命季历，明天瑞也。太伯之吴，遂生源也。”天命难言，非圣人莫能见。舜、禹、契、后稷皆黄帝子孙也。黄帝策天命而治天下，德泽深後世，故其子孙皆复立为天子，是天之报有德也。人不知，以为氾从布衣匹夫起耳。夫布衣匹夫安能无故而起王天下乎？其有天命然。”</p><p>　　“黄帝後世何王天下之久远邪？”</p><p>　　曰：“传云天下之君王为万夫之黔首请赎民之命者帝，有福万世。黄帝是也。五政明则修礼义，因天时举兵征伐而利者王，有福千世。蜀王，黄帝後世也，至今在汉西南五千里，常来朝降，输献於汉，非以其先之有德，泽流後世邪？行道德岂可以忽秋哉！人君王者举而观之。汉大将军霍子孟名光者，亦黄帝後世也。此可为博闻远见者言，固难为浅闻者说也。何以言之？古诸侯以国为姓。霍者，国名也。武王封弟叔处於霍，後世晋献公灭霍公，後世为庶民，往来居平阳。平阳在河东，河东晋地，分为卫国。以诗言之，亦可为周世。周起后稷，后稷无父而生。以三代世传言之，后稷有父名高辛；高辛，黄帝曾孙。黄帝终始传曰：“汉兴百有馀年，有人不短不长，出燕之乡，持天下之政，时有婴兒主，欲行车。”霍将军者，本居平阳燕。臣为郎时，与方士考功会旗亭下，为臣言。岂不伟哉！”</p><p></p><p>　　高辛之胤，大启祯祥。脩己吞薏，石纽兴王。天命玄鸟，简秋生商。姜嫄履迹，祚流岐昌。俱膺历运，互有兴亡。风馀周召，刑措成康。出彘之後，诸侯日彊。</p><p><br></p>', '2147483647', '2147483647');
INSERT INTO `edms_book_chapter` VALUES ('5', '1', '1', '五帝本纪', '五帝本纪1', '黄帝者，少典之子，姓公孙，名曰轩辕。生而神灵，弱而能言，幼而徇齐，长而敦敏，成而聪明。轩辕之时，神农氏世衰。诸侯相侵伐，暴虐百姓，而神农氏弗能征。於是轩辕乃习用干戈，以征不享，诸侯咸来宾从。而蚩尤最为暴，莫能伐。', '<p>　　黄帝者，少典之子，姓公孙，名曰轩辕。生而神灵，弱而能言，幼而徇齐，长而敦敏，成而聪明。轩辕之时，神农氏世衰。诸侯相侵伐，暴虐百姓，而神农氏弗能征。於是轩辕乃习用干戈，以征不享，诸侯咸来宾从。而蚩尤最为暴，莫能伐。炎帝欲侵陵诸侯，诸侯咸归轩辕。轩辕乃修德振兵，治五气，蓺五种，抚万民，度四方，教熊罴貔貅貙虎，以与炎帝战於阪泉之野。三战，然後得其志。蚩尤作乱，不用帝命。於是黄帝乃徵师诸侯，与蚩尤战於涿鹿之野，遂禽杀蚩尤。而诸侯咸尊轩辕为天子，代神农氏，是为黄帝。天下有不顺者，黄帝从而征之，平者去之，披山通道，未尝宁居。</p><p>　　东至于海，登丸山，及岱宗。西至于空桐，登鸡头。南至于江，登熊、湘。北逐荤粥，合符釜山，而邑于涿鹿之阿。迁徙往来无常处，以师兵为营卫。官名皆以云命，为云师。置左右大监，监于万国。万国和，而鬼神山川封禅与为多焉。获宝鼎，迎日推筴。举风后、力牧、常先、大鸿以治民。顺天地之纪，幽明之占，死生之说，存亡之难。时播百穀草木，淳化鸟兽蟲蛾，旁罗日月星辰水波土石金玉，劳勤心力耳目，节用水火材物。有土德之瑞，故号黄帝。</p><p>　　黄帝二十五子，其得姓者十四人。</p><p>　　黄帝居轩辕之丘，而娶於西陵之女，是为嫘祖。嫘祖为黄帝正妃，生二子，其後皆有天下：其一曰玄嚣，是为青阳，青阳降居江水；其二曰昌意，降居若水。昌意娶蜀山氏女，曰昌仆，生高阳，高阳有圣德焉。黄帝崩，葬桥山。其孙昌意之子高阳立，是为帝颛顼也。</p><p>　　帝颛顼高阳者，黄帝之孙而昌意之子也。静渊以有谋，疏通而知事；养材以任地，载时以象天，依鬼神以制义，治气以教化，絜诚以祭祀。北至于幽陵，南至于交阯，西至于流沙，东至于蟠木。动静之物，大小之神，日月所照，莫不砥属。</p><p>　　帝颛顼生子曰穷蝉。颛顼崩，而玄嚣之孙高辛立，是为帝喾。</p><p>　　帝喾高辛者，黄帝之曾孙也。高辛父曰蟜极，蟜极父曰玄嚣，玄嚣父曰黄帝。自玄嚣与蟜极皆不得在位，至高辛即帝位。高辛於颛顼为族子。</p><p>　　高辛生而神灵，自言其名。普施利物，不於其身。聪以知远，明以察微。顺天之义，知民之急。仁而威，惠而信，脩身而天下服。取地之财而节用之，抚教万民而利诲之，历日月而迎送之，明鬼神而敬事之。其色郁郁，其德嶷嶷。其动也时，其服也士。帝喾溉执中而遍天下，日月所照，风雨所至，莫不从服。</p><p>　　帝喾娶陈锋氏女，生放勋。娶娵訾氏女，生挚。帝喾崩，而挚代立。帝挚立，不善，而弟放勋立，是为帝尧。</p><p>　　帝尧者，放勋。其仁如天，其知如神。就之如日，望之如云。富而不骄，贵而不舒。黄收纯衣，彤车乘白马。能明驯德，以亲九族。九族既睦，便章百姓。百姓昭明，合和万国。</p><p>　　乃命羲、和，敬顺昊天，数法日月星辰，敬授民时。分命羲仲，居郁夷，曰旸谷。敬道日出，便程东作。日中，星鸟，以殷中春。其民析，鸟兽字微。申命羲叔，居南交。便程南为，敬致。日永，星火，以正中夏。其民因，鸟兽希革。申命和仲，居西土，曰昧谷。敬道日入，便程西成。夜中，星虚，以正中秋。其民夷易，鸟兽毛毨。申命和叔；居北方，曰幽都。便在伏物。日短，星昴，以正中冬。其民燠，鸟兽氄毛。岁三百六十六日，以闰月正四时。信饬百官，众功皆兴。</p><p>　　尧曰：“谁可顺此事？”放齐曰：“嗣子丹硃开明。”尧曰：“吁！顽凶，不用。”尧又曰：“谁可者？”讙兜曰：“共工旁聚布功，可用。”尧曰：“共工善言，其用僻，似恭漫天，不可。”尧又曰：“嗟，四岳，汤汤洪水滔天，浩浩怀山襄陵，下民其忧，有能使治者？”皆曰鲧可。尧曰：“鲧负命毁族，不可。”岳曰：“异哉，试不可用而已。”尧於是听岳用鲧。九岁，功用不成。</p><p>　　尧曰：“嗟！四岳：朕在位七十载，汝能庸命，践朕位？”岳应曰：“鄙德忝帝位。”尧曰：“悉举贵戚及疏远隐匿者。”众皆言於尧曰：“有矜在民间，曰虞舜。”尧曰：“然，朕闻之。其何如？”岳曰：“盲者子。父顽，母嚚，弟傲，能和以孝，烝烝治，不至奸。”尧曰：“吾其试哉。”於是尧妻之二女，观其德於二女。舜饬下二女於妫汭，如妇礼。尧善之，乃使舜慎和五典，五典能从。乃遍入百官，百官时序。宾於四门，四门穆穆，诸侯远方宾客皆敬。尧使舜入山林川泽，暴风雷雨，舜行不迷。尧以为圣，召舜曰：“女谋事至而言可绩，三年矣。女登帝位。”舜让於德不怿。正月上日，舜受终於文祖。文祖者，尧大祖也。</p><p>　　於是帝尧老，命舜摄行天子之政，以观天命。舜乃在璿玑玉衡，以齐七政。遂类于上帝，禋于六宗，望于山川，辩于群神。揖五瑞，择吉月日，见四岳诸牧，班瑞。岁二月，东巡狩，至於岱宗，祡，望秩於山川。遂见东方君长，合时月正日，同律度量衡，脩五礼五玉三帛二生一死为挚，如五器，卒乃复。五月，南巡狩；八月，西巡狩；十一月，北巡狩：皆如初。归，至于祖祢庙，用特牛礼。五岁一巡狩，群后四朝。遍告以言，明试以功，车服以庸。肇十有二州，决川。象以典刑，流宥五刑，鞭作官刑，扑作教刑，金作赎刑。眚灾过，赦；怙终贼，刑。钦哉，钦哉，惟刑之静哉！</p><p>　　讙兜进言共工，尧曰不可而试之工师，共工果淫辟。四岳举鲧治鸿水，尧以为不可，岳彊请试之，试之而无功，故百姓不便。三苗在江淮、荆州数为乱。於是舜归而言於帝，请流共工於幽陵，以变北狄；放驩兜於崇山，以变南蛮；迁三苗於三危，以变西戎；殛鲧於羽山，以变东夷：四罪而天下咸服。</p><p>　　尧立七十年得舜，二十年而老，令舜摄行天子之政，荐之於天。尧辟位凡二十八年而崩。百姓悲哀，如丧父母。三年，四方莫举乐，以思尧。尧知子丹硃之不肖，不足授天下，於是乃权授舜。授舜，则天下得其利而丹硃病；授丹硃，则天下病而丹硃得其利。尧曰“终不以天下之病而利一人”，而卒授舜以天下。尧崩，三年之丧毕，舜让辟丹硃於南河之南。诸侯朝觐者不之丹硃而之舜，狱讼者不之丹硃而之舜，讴歌者不讴歌丹硃而讴歌舜。舜曰“天也”，夫而後之中国践天子位焉，是为帝舜。</p><p>　　虞舜者，名曰重华。重华父曰瞽叟，瞽叟父曰桥牛，桥牛父曰句望，句望父曰敬康，敬康父曰穷蝉，穷蝉父曰帝颛顼，颛顼父曰昌意：以至舜七世矣。自从穷蝉以至帝舜，皆微为庶人。</p><p>　　舜父瞽叟盲，而舜母死，瞽叟更娶妻而生象，象傲。瞽叟爱後妻子，常欲杀舜，舜避逃；及有小过，则受罪。顺事父及後母与弟，日以笃谨，匪有解。</p><p>　　舜，冀州之人也。舜耕历山，渔雷泽，陶河滨，作什器於寿丘，就时於负夏。舜父瞽叟顽，母嚚，弟象傲，皆欲杀舜。舜顺適不失子道，兄弟孝慈。欲杀，不可得；即求，尝在侧。</p><p>　　舜年二十以孝闻。三十而帝尧问可用者，四岳咸荐虞舜，曰可。於是尧乃以二女妻舜以观其内，使九男与处以观其外。舜居妫汭，内行弥谨。尧二女不敢以贵骄事舜亲戚，甚有妇道。尧九男皆益笃。舜耕历山，历山之人皆让畔；渔雷泽，雷泽上人皆让居；陶河滨，河滨器皆不苦窳。一年而所居成聚，二年成邑，三年成都。尧乃赐舜絺衣，与琴，为筑仓廪，予牛羊。瞽叟尚复欲杀之，使舜上涂廪，瞽叟从下纵火焚廪。舜乃以两笠自扞而下，去，得不死。後瞽叟又使舜穿井，舜穿井为匿空旁出。舜既入深，瞽叟与象共下土实井，舜从匿空出，去。瞽叟、象喜，以舜为已死。象曰“本谋者象。”象与其父母分，於是曰：“舜妻尧二女，与琴，象取之。牛羊仓廪予父母。”象乃止舜宫居，鼓其琴。舜往见之。象鄂不怿，曰：“我思舜正郁陶！”舜曰：“然，尔其庶矣！”舜复事瞽叟爱弟弥谨。於是尧乃试舜五典百官，皆治。</p><p>　　昔高阳氏有才子八人，世得其利，谓之“八恺”。高辛氏有才子八人，世谓之“八元”。此十六族者，世济其美，不陨其名。至於尧，尧未能举。舜举八恺，使主后土，以揆百事，莫不时序。举八元，使布五教于四方，父义，母慈，兄友，弟恭，子孝，内平外成。</p><p>　　昔帝鸿氏有不才子，掩义隐贼，好行凶慝，天下谓之浑沌。少暤氏有不才子，毁信恶忠，崇饰恶言，天下谓之穷奇。颛顼氏有不才子，不可教训，不知话言，天下谓之檮杌。此三族世忧之。至于尧，尧未能去。缙云氏有不才子，贪于饮食，冒于货贿，天下谓之饕餮。天下恶之，比之三凶。舜宾於四门，乃流四凶族，迁于四裔，以御螭魅，於是四门辟，言毋凶人也。</p><p>　　舜入于大麓，烈风雷雨不迷，尧乃知舜之足授天下。尧老，使舜摄行天子政，巡狩。舜得举用事二十年，而尧使摄政。摄政八年而尧崩。三年丧毕，让丹硃，天下归舜。而禹、皋陶、契、后稷、伯夷、夔、龙、倕、益、彭祖自尧时而皆举用，未有分职。於是舜乃至於文祖，谋于四岳，辟四门，明通四方耳目，命十二牧论帝德，行厚德，远佞人，则蛮夷率服。舜谓四岳曰：“有能奋庸美尧之事者，使居官相事？”皆曰：“伯禹为司空，可美帝功。”舜曰：“嗟，然！禹，汝平水土，维是勉哉。”禹拜稽首，让於稷、契与皋陶。舜曰：“然，往矣。”舜曰：“弃，黎民始饥，汝后稷播时百穀。”舜曰：“契，百姓不亲，五品不驯，汝为司徒，而敬敷五教，在宽。”舜曰：“皋陶，蛮夷猾夏，寇贼奸轨，汝作士，五刑有服，五服三就；五流有度，五度三居：维明能信。”舜曰：“谁能驯予工？”皆曰垂可。於是以垂为共工。舜曰：“谁能驯予上下草木鸟兽？”皆曰益可。於是以益为朕虞。益拜稽首，让于诸臣硃虎、熊罴。舜曰：“往矣，汝谐。”遂以硃虎、熊罴为佐。舜曰：“嗟！四岳，有能典朕三礼？”皆曰伯夷可。舜曰：“嗟！伯夷，以汝为秩宗，夙夜维敬，直哉维静絜。”伯夷让夔、龙。舜曰：“然。以夔为典乐，教稺子，直而温，宽而栗，刚而毋虐，简而毋傲；诗言意，歌长言，声依永，律和声，八音能谐，毋相夺伦，神人以和。”夔曰：“於！予击石拊石，百兽率舞。”舜曰：“龙，朕畏忌谗说殄伪，振惊朕众，命汝为纳言，夙夜出入朕命，惟信。”舜曰：“嗟！女二十有二人，敬哉，惟时相天事。”三岁一考功，三考绌陟，远近众功咸兴。分北三苗。</p><p>　　此二十二人咸成厥功：皋陶为大理，平，民各伏得其实；伯夷主礼，上下咸让；垂主工师，百工致功；益主虞，山泽辟；弃主稷，百穀时茂；契主司徒，百姓亲和；龙主宾客，远人至；十二牧行而九州莫敢辟违；唯禹之功为大，披九山，通九泽，决九河，定九州，各以其职来贡，不失厥宜。方五千里，至于荒服。南抚交阯、北发，西戎、析枝、渠廋、氐、羌，北山戎、发、息慎，东长、鸟夷，四海之内咸戴帝舜之功。於是禹乃兴九招之乐，致异物，凤皇来翔。天下明德皆自虞帝始。</p><p>　　舜年二十以孝闻，年三十尧举之，年五十摄行天子事，年五十八尧崩，年六十一代尧践帝位。践帝位三十九年，南巡狩，崩於苍梧之野。葬於江南九疑，是为零陵。舜之践帝位，载天子旗，往朝父瞽叟，夔夔唯谨，如子道。封弟象为诸侯。舜子商均亦不肖，舜乃豫荐禹於天。十七年而崩。三年丧毕，禹亦乃让舜子，如舜让尧子。诸侯归之，然後禹践天子位。尧子丹硃，舜子商均，皆有疆土，以奉先祀。服其服，礼乐如之。以客见天子，天子弗臣，示不敢专也。</p><p>　　自黄帝至舜、禹，皆同姓而异其国号，以章明德。故黄帝为有熊，帝颛顼为高阳，帝喾为高辛，帝尧为陶唐，帝舜为有虞。帝禹为夏后而别氏，姓姒氏。契为商，姓子氏。弃为周，姓姬氏。</p><p>　　太史公曰：学者多称五帝，尚矣。然尚书独载尧以来；而百家言黄帝，其文不雅驯，荐绅先生难言之。孔子所传宰予问五帝德及帝系姓，儒者或不传。余尝西至空桐，北过涿鹿，东渐於海，南浮江淮矣，至长老皆各往往称黄帝、尧、舜之处，风教固殊焉，总之不离古文者近是。予观春秋、国语，其发明五帝德、帝系姓章矣，顾弟弗深考，其所表见皆不虚。书缺有间矣，其轶乃时时见於他说。非好学深思，心知其意，固难为浅见寡闻道也。余并论次，择其言尤雅者，故著为本纪书首。</p><p>　　帝出少典，居于轩丘。既代炎历，遂禽蚩尤。高阳嗣位，静深有谋。小大远近，莫不怀柔。爰洎帝喾，列圣同休。帝挚之弟，其号放勋。就之如日，望之如云。郁夷东作，昧谷西曛。明扬仄陋，玄德升闻。能让天下，贤哉二君！</p><p><br></p><p><br></p>', '2147483647', '2147483647');
INSERT INTO `edms_book_chapter` VALUES ('6', '1', '1', '夏本纪', '', '', '', '2147483647', '2147483647');
INSERT INTO `edms_book_chapter` VALUES ('7', '1', '1', '殷本纪', '', '', '', '2147483647', '2147483647');
INSERT INTO `edms_book_chapter` VALUES ('8', '1', '1', '周本纪', '', '', '', '2147483647', '2147483647');
INSERT INTO `edms_book_chapter` VALUES ('9', '1', '1', '秦本纪', '', '', '', '2147483647', '2147483647');

-- ----------------------------
-- Table structure for edms_book_section
-- ----------------------------
DROP TABLE IF EXISTS `edms_book_section`;
CREATE TABLE `edms_book_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图书分卷ID',
  `book_id` int(11) NOT NULL COMMENT '图书ID',
  `title` varchar(200) NOT NULL COMMENT '章节标题',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '章节描述',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_at` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='分卷';

-- ----------------------------
-- Records of edms_book_section
-- ----------------------------
INSERT INTO `edms_book_section` VALUES ('1', '1', '十二本纪', '', '2147483647', '2147483647');
INSERT INTO `edms_book_section` VALUES ('3', '2', '1', '2', '2147483647', '2147483647');
INSERT INTO `edms_book_section` VALUES ('4', '2', '电话', '1', '2147483647', '2147483647');
INSERT INTO `edms_book_section` VALUES ('5', '1', '十表', '为政篇', '2147483647', '2147483647');

-- ----------------------------
-- Table structure for edms_category
-- ----------------------------
DROP TABLE IF EXISTS `edms_category`;
CREATE TABLE `edms_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '上级ID',
  `module_id` int(11) NOT NULL COMMENT '模型ID',
  `title` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'seo_标题',
  `alias` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '英文别名',
  `keywords` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'seo_关键词',
  `description` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'seo_栏目描述',
  `content` text COLLATE utf8_bin NOT NULL COMMENT '栏目内容',
  `icon` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '字体图标',
  `imgage_icon` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '栏目图标',
  `list_template` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '列表模版',
  `content_template` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '文档模版',
  `comment_template` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '评论模版',
  `type` tinyint(1) DEFAULT '0' COMMENT '0 列表模式  1 封面模式',
  `reorder` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 显示 1隐藏',
  `limit` int(11) NOT NULL DEFAULT '15' COMMENT '每页显示数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='文档分类';

-- ----------------------------
-- Records of edms_category
-- ----------------------------
INSERT INTO `edms_category` VALUES ('1', '0', '1', '新闻', 'xinwen', '新闻', '新闻', '', '', '', '', '', '', '1', '999', '0', '15');
INSERT INTO `edms_category` VALUES ('2', '0', '1', '科技', 'keji', '科技', '科技', '', '', '', '', '', '', '1', '998', '0', '15');
INSERT INTO `edms_category` VALUES ('3', '1', '1', '国内新闻', 'guonei', '国内新闻', '国内新闻', '', '', '', '', '', '', '0', '0', '0', '15');
INSERT INTO `edms_category` VALUES ('4', '1', '1', '社会', 'shehui', '社会', '社会', '', '', '', '', '', '', '0', '0', '0', '15');
INSERT INTO `edms_category` VALUES ('5', '2', '1', '互联网', 'hulianwang', '', '', '', '', '', '', '', '', '0', '1', '0', '15');
INSERT INTO `edms_category` VALUES ('6', '0', '2', '图书连载', 'book', '', '', '', '', '', '', '', '', '1', '997', '0', '15');
INSERT INTO `edms_category` VALUES ('7', '6', '2', '玄幻', 'xuanhuang', '', '', '', '', '', 'book_list', 'book', '', '0', '1', '0', '15');
INSERT INTO `edms_category` VALUES ('8', '0', '3', '关于我们', 'about', '关于我们', '哈哈', 0x3C703E313233313133313C2F703E, '', '', '', '', '', '0', '996', '0', '15');
INSERT INTO `edms_category` VALUES ('9', '4', '1', '111', '111', '', '', '', '', '', '', '', '', '0', '0', '0', '15');

-- ----------------------------
-- Table structure for edms_config
-- ----------------------------
DROP TABLE IF EXISTS `edms_config`;
CREATE TABLE `edms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL COMMENT 'key',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '配置项名称',
  `value` tinytext NOT NULL COMMENT '值',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据类型\n1字符串 2 json 3数字',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 默认 1 系统配置',
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '标志 \n0默认\n1网站信息 \n2模版 3注册',
  `reorder` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='配置表';

-- ----------------------------
-- Records of edms_config
-- ----------------------------
INSERT INTO `edms_config` VALUES ('1', 'link', '友情链接分类', '{\"1\": \"分类1\", \"2\": \"分类2\",\"3\": \"分类3\"}', '友情链接分类', '2', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('2', 'article_status', '文章默认状态', '1', '0 为未审核 1 审核通过', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('3', 'article_limit', '每页文章显示数量', '10', '', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('4', 'theme', '当前使用模版', 'default', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('5', 'name', '您的网站名称', '您的网站名称', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('6', 'domain', '网站域名', 'http://edms.kitesky.com', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('7', 'title', '网站标题', '您的网站标题', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('8', 'keywords', '网站关键词', '网站关键词', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('9', 'description', '网站描述', '网站描述', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('10', 'copyright', '版权信息', 'Copyright © 2016-2017 系统演示网站. All rights reserved.', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('11', 'statistics', '统计代码', '1', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('12', 'login_tpl', '用户登陆模版', 'login', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('13', 'register_tpl', '注册用户模版', 'register', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('14', 'forget_tpl', '忘记密码找回模版', 'forget', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('15', 'profile_tpl', '会员资料页面', 'profile', '', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('16', 'register', '是否开启注册', '0', '0 开启 1 关闭', '3', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('17', 'close', '是否关闭网站', '0', '0 开启 1 关闭', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('18', 'publish', '是否开启投稿', '0', '0 开启 1 关闭', '3', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('19', 'comment', '是否开启评论', '0', '0 开启 1 关闭', '3', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('20', 'article_list_tpl', '文章列表模版', 'article_list', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('21', 'article_content_tpl', '文章内容页模版', 'article', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('22', 'tag_tpl', 'Tag文章列表模版', 'tag', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('23', 'icp', '备案号', '1', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('24', 'captcha', '验证码配置', '{\"useZh\":\"0\",\"useCurve\":\"0\",\"fontSize\":\"14\",\"imageH\":\"30\",\"imageW\":\"100\",\"length\":\"4\"}', '{\"useZh\":\"0\",\"useCurve\":\"0\",\"fontSize\":\"14\",\"imageH\":\"30\",\"imageW\":\"100\",\"length\":\"4\"}', '2', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('25', 'upload', '图片文件保存目录', 'upload/image', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('26', 'thumb', '生成缩略图方案', '2', '1标识缩略图等比例缩放类型,\r\n2标识缩略图缩放后填充类型,\r\n3标识缩略图居中裁剪类型,\r\n4标识缩略图左上角裁剪类型,\r\n5标识缩略图右下角裁剪类型,\r\n6标识缩略图固定尺寸缩放类型', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('27', 'water', '生成水印方案', '{\"open\":\"0\",\"position\":\"5\",\"quality\":\"50\",\"scan\":\"0\",\"logo\":\"\"}', '1标识左上角水印,\r\n2标识上居中水印,\r\n3标识右上角水印,\r\n4标识左居中水印,\r\n5标识居中水印,\r\n6标识右居中水印,\r\n7标识左下角水印,\r\n8标识下居中水印,\r\n9标识右下角水印', '2', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('28', 'open_captcha', '开启验证码', '[\"5\"]', '1前台注册\r\n2前台登陆\r\n3会员投稿\r\n4文档评论\r\n5后台登陆', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('29', 'index_tpl', '首页模版', 'index', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('30', 'member_tpl', '会员个人页面', 'member', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('31', 'my_tpl', '用户中心', 'my', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('32', 'message_tpl', '我的信息模版', 'my_message', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('33', 'read_message_tpl', '信息详情模版', 'read_message', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('34', 'my_article_tpl', '我的文章', 'my_article', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('35', 'my_comment_tpl', '我的评论', 'my_comment', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('36', 'my_favorite_tpl', '我的收藏', 'my_favorite', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('37', 'my_setting_tpl', '我的资料', 'my_setting', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('38', 'address', '地址', '成都市青羊区东二路', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('39', 'contact', '联系方式', '028-66668888', '', '1', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('40', 'register_verify', '注册验证', '2', '3手机短信验证\r\n2邮件验证\r\n1后台审核\r\n0不验证', '3', '1', '1', '0');
INSERT INTO `edms_config` VALUES ('41', 'book_list_tpl', '图书列表模版', 'book_list', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('42', 'book_content_tpl', '图书内容页模版', 'book', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('43', 'book_chapter_tpl', '图书章节模版', 'chapter', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('44', 'article_comment_tpl', '文章评论模版', 'article_comment', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('45', 'book_limit', '每页图书显示数量', '10', '', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('46', 'comment_limit', '每页评论显示数量', '10', '', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('47', 'message_limit', '每页消息显示数量', '10', '', '3', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('48', 'search_tpl', '搜索页模版', 'search', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('49', 'mail', '邮件配置', '{\"mail_host\":\"smtp.163.com\",\"mail_port\":\"25\",\"mail_ssl\":\"0\",\"mail_user\":\"nsssh@163.com\",\"mail_pwd\":\"wangzheng\"}', '', '1', '1', '0', '0');
INSERT INTO `edms_config` VALUES ('50', 'single_tpl', '单页面模版', 'single', '', '1', '1', '0', '0');

-- ----------------------------
-- Table structure for edms_feedback
-- ----------------------------
DROP TABLE IF EXISTS `edms_feedback`;
CREATE TABLE `edms_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '反馈ID编号',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '姓名',
  `phone` varchar(32) NOT NULL DEFAULT '' COMMENT '电话',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '发送时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for edms_hook
-- ----------------------------
DROP TABLE IF EXISTS `edms_hook`;
CREATE TABLE `edms_hook` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `hook` varchar(64) NOT NULL COMMENT '钩子标识',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '钩子名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_hook
-- ----------------------------

-- ----------------------------
-- Table structure for edms_link
-- ----------------------------
DROP TABLE IF EXISTS `edms_link`;
CREATE TABLE `edms_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型: 0 文字, 1 图片',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '网站名称',
  `url` varchar(64) NOT NULL DEFAULT '' COMMENT '网站地址',
  `logo` varchar(255) NOT NULL DEFAULT '' COMMENT 'logo地址',
  `remark` varchar(64) NOT NULL DEFAULT '' COMMENT '备注说明',
  `reorder` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态： 0 待审 1 审核',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='友情链接';

-- ----------------------------
-- Records of edms_link
-- ----------------------------
INSERT INTO `edms_link` VALUES ('1', '1', '百度', 'http://www.baidu.com', 'https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=231789442,2840259962&fm=80&w=179&h=119&img.JPEG', '全球最大的中文搜索引擎', '1', '1');
INSERT INTO `edms_link` VALUES ('2', '0', '网易', 'http://mail.163.com/', '', '1', '0', '1');

-- ----------------------------
-- Table structure for edms_message
-- ----------------------------
DROP TABLE IF EXISTS `edms_message`;
CREATE TABLE `edms_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `to_uid` int(11) NOT NULL COMMENT '收信人 UID',
  `from_uid` int(11) NOT NULL COMMENT '发信人 UID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '短信标题',
  `content` varchar(255) NOT NULL COMMENT '短信内容',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '信息类型  0系统消息 1私信',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0未读 1已读',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '发送时间',
  `update_at` int(10) NOT NULL DEFAULT '0' COMMENT '阅读时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_message
-- ----------------------------
INSERT INTO `edms_message` VALUES ('1', '1', '1', '123', '123', '2', '1', '2147483647', '2147483647');
INSERT INTO `edms_message` VALUES ('2', '1', '1', '2222', '3333', '1', '1', '2147483647', '2147483647');
INSERT INTO `edms_message` VALUES ('3', '1', '0', '好地方', '\r\nHe skimmed the pages quickly, then read them again more carefully. \r\n他先快速地浏览页面，然后再细细阅读。', '1', '1', '2147483647', '2147483647');

-- ----------------------------
-- Table structure for edms_module
-- ----------------------------
DROP TABLE IF EXISTS `edms_module`;
CREATE TABLE `edms_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` varchar(64) NOT NULL COMMENT '模型名称',
  `table_name` varchar(64) NOT NULL COMMENT '模型表名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_module
-- ----------------------------
INSERT INTO `edms_module` VALUES ('1', '文章', 'article');
INSERT INTO `edms_module` VALUES ('2', '书籍', 'book');
INSERT INTO `edms_module` VALUES ('3', '单页', 'page');

-- ----------------------------
-- Table structure for edms_plugin
-- ----------------------------
DROP TABLE IF EXISTS `edms_plugin`;
CREATE TABLE `edms_plugin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) NOT NULL COMMENT '插件名或标识',
  `description` varchar(64) NOT NULL COMMENT '插件描述',
  `config` tinytext NOT NULL COMMENT '配置',
  `author` varchar(64) NOT NULL DEFAULT '' COMMENT '作者',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '版本号',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0 启用 1 禁用',
  `create_at` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for edms_slider
-- ----------------------------
DROP TABLE IF EXISTS `edms_slider`;
CREATE TABLE `edms_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(64) NOT NULL DEFAULT '' COMMENT 'caption',
  `image` varchar(255) NOT NULL COMMENT '图片路径',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `url` varchar(64) NOT NULL DEFAULT '' COMMENT '图片链接地址',
  `reorder` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0显示 1隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='图片轮播	';

-- ----------------------------
-- Records of edms_slider
-- ----------------------------
INSERT INTO `edms_slider` VALUES ('1', '第一张', 'upload/image/20170520/85b844c445fc6d38104ad840649c1be8.jpg', '1', '', '0', '0');
INSERT INTO `edms_slider` VALUES ('2', '第二张', 'upload/image/20170520/1f2859869341a99bebd9ae005629a367.jpg', '', '', '0', '0');
INSERT INTO `edms_slider` VALUES ('3', '第三张', 'upload/image/20170520/f42c147a9d05b1cf0b41ba875017916b.jpg', '', '', '0', '0');

-- ----------------------------
-- Table structure for edms_upload_file
-- ----------------------------
DROP TABLE IF EXISTS `edms_upload_file`;
CREATE TABLE `edms_upload_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '上传用户UID',
  `hash` varchar(255) NOT NULL DEFAULT '' COMMENT '图标标题',
  `path` varchar(255) NOT NULL COMMENT '图片路径',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '存储方式1 本地 2 阿里云OSS',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '上传日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of edms_upload_file
-- ----------------------------

-- ----------------------------
-- Table structure for edms_user
-- ----------------------------
DROP TABLE IF EXISTS `edms_user`;
CREATE TABLE `edms_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL COMMENT '所属用户组',
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `password` varchar(72) NOT NULL COMMENT '密码',
  `nickname` varchar(64) NOT NULL DEFAULT '' COMMENT '昵称',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别 0 未知 1 男性 2 女性 ',
  `email` varchar(64) NOT NULL,
  `phone` char(16) NOT NULL COMMENT '电话号码',
  `avatar` varchar(255) NOT NULL,
  `motto` varchar(255) NOT NULL DEFAULT '' COMMENT '格言主题句',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT ' 0 前台用户 1后台用户',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户状态 0 正常 1禁用',
  `verify` tinyint(1) NOT NULL DEFAULT '0' COMMENT '核实信息 0 未认证 1 核实',
  `security` varchar(32) NOT NULL COMMENT '安全校验码 32位MD5',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '分数',
  `create_at` int(10) NOT NULL DEFAULT '0',
  `update_at` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of edms_user
-- ----------------------------
INSERT INTO `edms_user` VALUES ('1', '2', 'kite', '$2y$10$zjdHqd3A/bURSKey9Bf41OtTJJVPOfzVfYbmmpc8G.YVtGBEq5EEm', '风筝', '2', 'nsssh@163.com', '18780221108', 'upload/avatar/1.png', '生如逆旅,一苇以航', '1', '0', '1', '', '0', '2147483647', '2147483647');
INSERT INTO `edms_user` VALUES ('2', '1', 'admin', '$2y$10$1T/BwigfucS1/kF3XfHyIuIV.GQflgXVAxPiyZDUzMNQIlogd3EG2', '', '1', 'admin@kitesky.com', '18780221109', 'upload/avatar/2.png', '', '1', '0', '1', '', '0', '2147483647', '2147483647');

-- ----------------------------
-- Table structure for edms_user_favorite
-- ----------------------------
DROP TABLE IF EXISTS `edms_user_favorite`;
CREATE TABLE `edms_user_favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '收藏用户UID',
  `collect` int(11) NOT NULL COMMENT '收藏的内容ID编号',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1文章 2图书',
  `create_at` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='上传图片记录';

-- ----------------------------
-- Records of edms_user_favorite
-- ----------------------------
INSERT INTO `edms_user_favorite` VALUES ('1', '1', '1', '1', '2147483647');
INSERT INTO `edms_user_favorite` VALUES ('2', '1', '2', '1', '2147483647');
INSERT INTO `edms_user_favorite` VALUES ('3', '1', '1', '2', '2147483647');
INSERT INTO `edms_user_favorite` VALUES ('4', '1', '2', '2', '2147483647');

-- ----------------------------
-- Table structure for edms_user_login_log
-- ----------------------------
DROP TABLE IF EXISTS `edms_user_login_log`;
CREATE TABLE `edms_user_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL,
  `login_time` int(10) NOT NULL DEFAULT '0' COMMENT '登陆时间',
  `login_ip` varchar(32) NOT NULL COMMENT '登陆IP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8 COMMENT='用户登陆日志';

-- ----------------------------
-- Records of edms_user_login_log
-- ----------------------------
INSERT INTO `edms_user_login_log` VALUES ('1', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('2', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('3', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('4', '1', '2147483647', '222.212.132.47');
INSERT INTO `edms_user_login_log` VALUES ('5', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('6', '1', '2147483647', '222.212.132.47');
INSERT INTO `edms_user_login_log` VALUES ('7', '1', '2147483647', '222.212.132.47');
INSERT INTO `edms_user_login_log` VALUES ('8', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('9', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('10', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('11', '1', '2147483647', '182.139.181.184');
INSERT INTO `edms_user_login_log` VALUES ('12', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('13', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('14', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('15', '1', '2147483647', '182.139.181.184');
INSERT INTO `edms_user_login_log` VALUES ('16', '2', '2147483647', '117.136.63.166');
INSERT INTO `edms_user_login_log` VALUES ('17', '2', '2147483647', '101.95.186.150');
INSERT INTO `edms_user_login_log` VALUES ('18', '1', '2147483647', '171.221.241.68');
INSERT INTO `edms_user_login_log` VALUES ('19', '2', '2147483647', '218.19.50.110');
INSERT INTO `edms_user_login_log` VALUES ('20', '2', '2147483647', '106.121.74.240');
INSERT INTO `edms_user_login_log` VALUES ('21', '1', '2147483647', '182.139.181.184');
INSERT INTO `edms_user_login_log` VALUES ('22', '2', '2147483647', '117.62.23.194');
INSERT INTO `edms_user_login_log` VALUES ('23', '2', '2147483647', '182.139.180.191');
INSERT INTO `edms_user_login_log` VALUES ('24', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('25', '1', '2147483647', '182.139.180.92');
INSERT INTO `edms_user_login_log` VALUES ('26', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('27', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('28', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('29', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('30', '1', '2147483647', '182.139.180.92');
INSERT INTO `edms_user_login_log` VALUES ('31', '2', '2147483647', '182.139.180.92');
INSERT INTO `edms_user_login_log` VALUES ('32', '1', '2147483647', '182.138.212.73');
INSERT INTO `edms_user_login_log` VALUES ('33', '1', '2147483647', '182.138.212.73');
INSERT INTO `edms_user_login_log` VALUES ('34', '1', '2147483647', '222.212.133.31');
INSERT INTO `edms_user_login_log` VALUES ('35', '1', '2147483647', '222.212.133.31');
INSERT INTO `edms_user_login_log` VALUES ('36', '1', '2147483647', '182.138.212.73');
INSERT INTO `edms_user_login_log` VALUES ('37', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('38', '1', '2147483647', '222.212.134.101');
INSERT INTO `edms_user_login_log` VALUES ('39', '2', '2147483647', '112.65.19.2');
INSERT INTO `edms_user_login_log` VALUES ('40', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('41', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('42', '2', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('43', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('44', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('45', '2', '2147483647', '36.110.69.22');
INSERT INTO `edms_user_login_log` VALUES ('46', '2', '2147483647', '39.64.197.118');
INSERT INTO `edms_user_login_log` VALUES ('47', '1', '2147483647', '171.217.109.159');
INSERT INTO `edms_user_login_log` VALUES ('48', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('49', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('50', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('51', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('52', '2', '2147483647', '182.138.212.60');
INSERT INTO `edms_user_login_log` VALUES ('53', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('54', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('55', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('56', '2', '2147483647', '222.209.47.88');
INSERT INTO `edms_user_login_log` VALUES ('57', '1', '2147483647', '222.209.47.88');
INSERT INTO `edms_user_login_log` VALUES ('58', '2', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('59', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('60', '1', '2147483647', '222.209.47.88');
INSERT INTO `edms_user_login_log` VALUES ('61', '2', '2147483647', '119.187.2.215');
INSERT INTO `edms_user_login_log` VALUES ('62', '1', '2147483647', '222.209.47.88');
INSERT INTO `edms_user_login_log` VALUES ('63', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('64', '2', '2147483647', '221.237.152.47');
INSERT INTO `edms_user_login_log` VALUES ('65', '2', '2147483647', '222.72.174.12');
INSERT INTO `edms_user_login_log` VALUES ('66', '2', '2147483647', '14.208.23.251');
INSERT INTO `edms_user_login_log` VALUES ('67', '2', '2147483647', '218.88.24.220');
INSERT INTO `edms_user_login_log` VALUES ('68', '1', '2147483647', '222.212.134.17');
INSERT INTO `edms_user_login_log` VALUES ('69', '1', '2147483647', '222.212.135.130');
INSERT INTO `edms_user_login_log` VALUES ('70', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('71', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('72', '2', '2147483647', '121.8.184.189');
INSERT INTO `edms_user_login_log` VALUES ('73', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('74', '2', '2147483647', '180.212.239.181');
INSERT INTO `edms_user_login_log` VALUES ('75', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('76', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('77', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('78', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('79', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('80', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('81', '1', '2147483647', '222.212.132.77');
INSERT INTO `edms_user_login_log` VALUES ('82', '2', '2147483647', '114.243.158.94');
INSERT INTO `edms_user_login_log` VALUES ('83', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('84', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('85', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('86', '2', '2147483647', '14.223.187.130');
INSERT INTO `edms_user_login_log` VALUES ('87', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('88', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('89', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('90', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('91', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('92', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('93', '1', '2147483647', '182.138.212.78');
INSERT INTO `edms_user_login_log` VALUES ('94', '1', '2147483647', '222.209.44.35');
INSERT INTO `edms_user_login_log` VALUES ('95', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('96', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('97', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('98', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('99', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('100', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('101', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('102', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('103', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('104', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('105', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('106', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('107', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('108', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('109', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('110', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('111', '1', '2147483647', '182.139.180.252');
INSERT INTO `edms_user_login_log` VALUES ('112', '2', '2147483647', '182.139.180.252');
INSERT INTO `edms_user_login_log` VALUES ('113', '2', '2147483647', '211.161.244.168');
INSERT INTO `edms_user_login_log` VALUES ('114', '2', '2147483647', '114.249.214.248');
INSERT INTO `edms_user_login_log` VALUES ('115', '2', '2147483647', '163.125.149.59');
INSERT INTO `edms_user_login_log` VALUES ('116', '2', '2147483647', '163.125.149.59');
INSERT INTO `edms_user_login_log` VALUES ('117', '2', '2147483647', '114.218.26.102');
INSERT INTO `edms_user_login_log` VALUES ('118', '2', '2147483647', '42.84.194.240');
INSERT INTO `edms_user_login_log` VALUES ('119', '2', '2147483647', '0.0.0.0');
INSERT INTO `edms_user_login_log` VALUES ('120', '2', '2147483647', '60.2.125.182');
INSERT INTO `edms_user_login_log` VALUES ('121', '2', '2147483647', '220.249.19.46');
INSERT INTO `edms_user_login_log` VALUES ('122', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('123', '2', '2147483647', '124.205.217.34');
INSERT INTO `edms_user_login_log` VALUES ('124', '2', '2147483647', '106.120.40.10');
INSERT INTO `edms_user_login_log` VALUES ('125', '2', '2147483647', '59.38.35.162');
INSERT INTO `edms_user_login_log` VALUES ('126', '2', '2147483647', '218.108.212.219');
INSERT INTO `edms_user_login_log` VALUES ('127', '2', '2147483647', '218.17.161.166');
INSERT INTO `edms_user_login_log` VALUES ('128', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('129', '2', '2147483647', '183.63.18.162');
INSERT INTO `edms_user_login_log` VALUES ('130', '2', '2147483647', '171.11.232.2');
INSERT INTO `edms_user_login_log` VALUES ('131', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('132', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('133', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('134', '2', '2147483647', '0.0.0.0');
INSERT INTO `edms_user_login_log` VALUES ('135', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('136', '2', '2147483647', '113.108.169.165');
INSERT INTO `edms_user_login_log` VALUES ('137', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('138', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('139', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('140', '2', '2147483647', '116.228.34.130');
INSERT INTO `edms_user_login_log` VALUES ('141', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('142', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('143', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('144', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('145', '2', '2147483647', '119.127.17.196');
INSERT INTO `edms_user_login_log` VALUES ('146', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('147', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('148', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('149', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('150', '1', '2147483647', '219.142.242.178');
INSERT INTO `edms_user_login_log` VALUES ('151', '1', '2147483647', '113.99.3.224');
INSERT INTO `edms_user_login_log` VALUES ('152', '1', '2147483647', '115.57.135.237');
INSERT INTO `edms_user_login_log` VALUES ('153', '2', '2147483647', '59.46.175.74');
INSERT INTO `edms_user_login_log` VALUES ('154', '1', '2147483647', '171.213.60.57');
INSERT INTO `edms_user_login_log` VALUES ('155', '1', '2147483647', '211.144.6.66');
INSERT INTO `edms_user_login_log` VALUES ('156', '1', '2147483647', '101.81.28.10');
INSERT INTO `edms_user_login_log` VALUES ('157', '1', '2147483647', '117.174.24.156');
INSERT INTO `edms_user_login_log` VALUES ('158', '2', '2147483647', '116.228.34.130');
INSERT INTO `edms_user_login_log` VALUES ('159', '1', '2147483647', '183.14.133.248');
INSERT INTO `edms_user_login_log` VALUES ('160', '1', '2147483647', '123.52.73.143');
INSERT INTO `edms_user_login_log` VALUES ('161', '1', '2147483647', '122.156.6.22');
INSERT INTO `edms_user_login_log` VALUES ('162', '1', '2147483647', '117.27.76.134');
INSERT INTO `edms_user_login_log` VALUES ('163', '1', '2147483647', '124.128.251.218');
INSERT INTO `edms_user_login_log` VALUES ('164', '1', '2147483647', '117.25.131.178');
INSERT INTO `edms_user_login_log` VALUES ('165', '1', '2147483647', '218.17.197.218');
INSERT INTO `edms_user_login_log` VALUES ('166', '1', '2147483647', '42.48.154.160');
INSERT INTO `edms_user_login_log` VALUES ('167', '1', '2147483647', '112.65.19.2');
INSERT INTO `edms_user_login_log` VALUES ('168', '1', '2147483647', '220.133.74.196');
INSERT INTO `edms_user_login_log` VALUES ('169', '1', '2147483647', '180.154.53.41');
INSERT INTO `edms_user_login_log` VALUES ('170', '2', '2147483647', '117.25.131.178');
INSERT INTO `edms_user_login_log` VALUES ('171', '1', '2147483647', '118.144.186.212');
INSERT INTO `edms_user_login_log` VALUES ('172', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('173', '2', '2147483647', '112.65.19.2');
INSERT INTO `edms_user_login_log` VALUES ('174', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('175', '1', '2147483647', '36.110.113.132');
INSERT INTO `edms_user_login_log` VALUES ('176', '1', '2147483647', '121.237.48.62');
INSERT INTO `edms_user_login_log` VALUES ('177', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('178', '2', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('179', '1', '2147483647', '115.204.91.61');
INSERT INTO `edms_user_login_log` VALUES ('180', '2', '2147483647', '112.65.19.2');
INSERT INTO `edms_user_login_log` VALUES ('181', '1', '2147483647', '113.109.42.28');
INSERT INTO `edms_user_login_log` VALUES ('182', '1', '2147483647', '61.154.12.206');
INSERT INTO `edms_user_login_log` VALUES ('183', '1', '2147483647', '113.116.203.228');
INSERT INTO `edms_user_login_log` VALUES ('184', '1', '2147483647', '182.109.20.220');
INSERT INTO `edms_user_login_log` VALUES ('185', '1', '2147483647', '103.218.216.130');
INSERT INTO `edms_user_login_log` VALUES ('186', '1', '2147483647', '171.221.139.195');
INSERT INTO `edms_user_login_log` VALUES ('187', '1', '2147483647', '219.133.66.79');
INSERT INTO `edms_user_login_log` VALUES ('188', '1', '2147483647', '122.193.46.146');
INSERT INTO `edms_user_login_log` VALUES ('189', '2', '2147483647', '183.156.124.250');
INSERT INTO `edms_user_login_log` VALUES ('190', '2', '2147483647', '116.22.54.3');
INSERT INTO `edms_user_login_log` VALUES ('191', '1', '2147483647', '113.47.181.115');
INSERT INTO `edms_user_login_log` VALUES ('192', '1', '2147483647', '221.133.242.138');
INSERT INTO `edms_user_login_log` VALUES ('193', '1', '2147483647', '59.37.20.6');
INSERT INTO `edms_user_login_log` VALUES ('194', '1', '2147483647', '59.37.20.6');
INSERT INTO `edms_user_login_log` VALUES ('195', '1', '2147483647', '36.5.14.29');
INSERT INTO `edms_user_login_log` VALUES ('196', '1', '2147483647', '14.116.37.238');
INSERT INTO `edms_user_login_log` VALUES ('197', '1', '2147483647', '121.69.67.230');
INSERT INTO `edms_user_login_log` VALUES ('198', '1', '2147483647', '222.174.115.93');
INSERT INTO `edms_user_login_log` VALUES ('199', '1', '2147483647', '180.173.48.251');
INSERT INTO `edms_user_login_log` VALUES ('200', '1', '2147483647', '14.154.185.82');
INSERT INTO `edms_user_login_log` VALUES ('201', '1', '2147483647', '36.57.138.236');
INSERT INTO `edms_user_login_log` VALUES ('202', '1', '2147483647', '36.57.138.236');
INSERT INTO `edms_user_login_log` VALUES ('203', '1', '2147483647', '120.85.77.106');
INSERT INTO `edms_user_login_log` VALUES ('204', '1', '2147483647', '58.248.228.37');
INSERT INTO `edms_user_login_log` VALUES ('205', '2', '2147483647', '113.106.194.238');
INSERT INTO `edms_user_login_log` VALUES ('206', '2', '2147483647', '113.106.194.238');
INSERT INTO `edms_user_login_log` VALUES ('207', '2', '2147483647', '113.106.194.238');
INSERT INTO `edms_user_login_log` VALUES ('208', '1', '2147483647', '27.154.237.126');
INSERT INTO `edms_user_login_log` VALUES ('209', '1', '2147483647', '180.173.48.251');
INSERT INTO `edms_user_login_log` VALUES ('210', '1', '2147483647', '122.189.196.232');
INSERT INTO `edms_user_login_log` VALUES ('211', '1', '2147483647', '180.212.216.249');
INSERT INTO `edms_user_login_log` VALUES ('212', '1', '2147483647', '121.231.226.5');
INSERT INTO `edms_user_login_log` VALUES ('213', '1', '2147483647', '60.208.54.124');
INSERT INTO `edms_user_login_log` VALUES ('214', '1', '2147483647', '106.7.153.19');
INSERT INTO `edms_user_login_log` VALUES ('215', '1', '2147483647', '113.108.186.130');
INSERT INTO `edms_user_login_log` VALUES ('216', '1', '2147483647', '202.104.136.194');
INSERT INTO `edms_user_login_log` VALUES ('217', '1', '2147483647', '119.131.170.203');
INSERT INTO `edms_user_login_log` VALUES ('218', '1', '2147483647', '122.70.186.202');
INSERT INTO `edms_user_login_log` VALUES ('219', '1', '2147483647', '117.159.5.130');
INSERT INTO `edms_user_login_log` VALUES ('220', '1', '2147483647', '110.177.53.129');
INSERT INTO `edms_user_login_log` VALUES ('221', '1', '2147483647', '203.152.92.26');
INSERT INTO `edms_user_login_log` VALUES ('222', '1', '2147483647', '183.213.60.43');
INSERT INTO `edms_user_login_log` VALUES ('223', '1', '2147483647', '183.193.23.196');
INSERT INTO `edms_user_login_log` VALUES ('224', '1', '2147483647', '222.35.226.143');
INSERT INTO `edms_user_login_log` VALUES ('225', '1', '2147483647', '58.52.179.200');
INSERT INTO `edms_user_login_log` VALUES ('226', '2', '2147483647', '58.52.179.200');
INSERT INTO `edms_user_login_log` VALUES ('227', '1', '2147483647', '14.116.38.245');
INSERT INTO `edms_user_login_log` VALUES ('228', '1', '2147483647', '118.192.104.73');
INSERT INTO `edms_user_login_log` VALUES ('229', '1', '2147483647', '101.95.186.150');
INSERT INTO `edms_user_login_log` VALUES ('230', '1', '2147483647', '113.0.80.204');
INSERT INTO `edms_user_login_log` VALUES ('231', '1', '2147483647', '111.198.142.144');
INSERT INTO `edms_user_login_log` VALUES ('232', '2', '2147483647', '58.59.201.80');
INSERT INTO `edms_user_login_log` VALUES ('233', '1', '2147483647', '61.153.254.98');
INSERT INTO `edms_user_login_log` VALUES ('234', '1', '2147483647', '61.153.254.98');
INSERT INTO `edms_user_login_log` VALUES ('235', '1', '2147483647', '101.69.248.150');
INSERT INTO `edms_user_login_log` VALUES ('236', '1', '2147483647', '101.81.48.230');
INSERT INTO `edms_user_login_log` VALUES ('237', '1', '2147483647', '112.96.173.190');
INSERT INTO `edms_user_login_log` VALUES ('238', '1', '2147483647', '182.110.132.247');
INSERT INTO `edms_user_login_log` VALUES ('239', '1', '2147483647', '121.204.250.140');
INSERT INTO `edms_user_login_log` VALUES ('240', '1', '2147483647', '182.42.64.135');
INSERT INTO `edms_user_login_log` VALUES ('241', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('242', '1', '2147483647', '171.221.254.149');
INSERT INTO `edms_user_login_log` VALUES ('243', '1', '2147483647', '183.238.188.194');
INSERT INTO `edms_user_login_log` VALUES ('244', '1', '2147483647', '182.150.59.66');
INSERT INTO `edms_user_login_log` VALUES ('245', '1', '2147483647', '58.248.22.20');
INSERT INTO `edms_user_login_log` VALUES ('246', '1', '2147483647', '58.248.24.252');
INSERT INTO `edms_user_login_log` VALUES ('247', '1', '2147483647', '1.204.115.21');
INSERT INTO `edms_user_login_log` VALUES ('248', '1', '2147483647', '202.112.81.111');
INSERT INTO `edms_user_login_log` VALUES ('249', '1', '2147483647', '119.131.76.109');
INSERT INTO `edms_user_login_log` VALUES ('250', '1', '2147483647', '223.153.95.62');
INSERT INTO `edms_user_login_log` VALUES ('251', '1', '2147483647', '58.30.134.6');
INSERT INTO `edms_user_login_log` VALUES ('252', '1', '2147483647', '218.201.251.90');
INSERT INTO `edms_user_login_log` VALUES ('253', '1', '2147483647', '111.112.72.17');
INSERT INTO `edms_user_login_log` VALUES ('254', '1', '2147483647', '223.71.139.19');
INSERT INTO `edms_user_login_log` VALUES ('255', '1', '2147483647', '61.140.25.25');
INSERT INTO `edms_user_login_log` VALUES ('256', '1', '2147483647', '101.81.230.17');
INSERT INTO `edms_user_login_log` VALUES ('257', '1', '2147483647', '119.187.120.188');
INSERT INTO `edms_user_login_log` VALUES ('258', '1', '2147483647', '120.194.1.94');
INSERT INTO `edms_user_login_log` VALUES ('259', '1', '2147483647', '219.136.75.90');
INSERT INTO `edms_user_login_log` VALUES ('260', '1', '2147483647', '61.142.113.82');
INSERT INTO `edms_user_login_log` VALUES ('261', '1', '2147483647', '58.221.146.34');
INSERT INTO `edms_user_login_log` VALUES ('262', '1', '2147483647', '220.181.171.121');
INSERT INTO `edms_user_login_log` VALUES ('263', '1', '2147483647', '219.137.142.104');
INSERT INTO `edms_user_login_log` VALUES ('264', '1', '2147483647', '202.127.115.84');
INSERT INTO `edms_user_login_log` VALUES ('265', '2', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('266', '1', '2147483647', '124.237.121.19');
INSERT INTO `edms_user_login_log` VALUES ('267', '1', '2147483647', '222.174.115.93');
INSERT INTO `edms_user_login_log` VALUES ('268', '1', '2147483647', '219.236.167.193');
INSERT INTO `edms_user_login_log` VALUES ('269', '2', '2147483647', '58.59.201.80');
INSERT INTO `edms_user_login_log` VALUES ('270', '1', '2147483647', '127.0.0.1');
INSERT INTO `edms_user_login_log` VALUES ('271', '1', '2147483647', '219.137.228.66');
INSERT INTO `edms_user_login_log` VALUES ('272', '1', '2147483647', '113.247.204.19');
INSERT INTO `edms_user_login_log` VALUES ('273', '1', '2147483647', '183.45.78.1');
INSERT INTO `edms_user_login_log` VALUES ('274', '1', '2147483647', '119.57.151.215');

-- ----------------------------
-- Table structure for edms_user_score_log
-- ----------------------------
DROP TABLE IF EXISTS `edms_user_score_log`;
CREATE TABLE `edms_user_score_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `type` tinyint(1) NOT NULL COMMENT '积分变动类型\n1注册赠送\n2签到\n3系统\n4兑换',
  `score` int(11) NOT NULL COMMENT '获取积分数量',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '获取积分说明',
  `create_at` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户积分记录表';

-- ----------------------------
-- Records of edms_user_score_log
-- ----------------------------
