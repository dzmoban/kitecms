<?php
namespace app\common\validate;
use think\Validate;

class CommentValidate extends Validate
{
    protected $rule = [
        'content' => 'require|max:255',
    ];

    protected $message = [
        'content.require'  => '标题名称必须',
        'content.max'      => '标题名称最多不能超过255个字符',
    ];
    
    protected $scene = [
        'add'   =>  ['content'],
        'edit'  =>  ['content'],
    ];
}