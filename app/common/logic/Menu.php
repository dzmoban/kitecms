<?php
namespace app\common\logic;

use think\Model;

class Menu extends Model
{
    /**
     * 后台菜单
     *
     * $class string 导航高亮css ，class名
     * @return array
     */
    function createMenu($class = 'active')
    {
        // 查询当前路由
        $request = \think\Request::instance();
        $map['module']     = strtolower($request->module());
        $map['controller'] = strtolower($request->controller());
        $map['action']     = strtolower($request->action());
        $rule = \app\common\model\AuthRule::getInfo($map);

        // 获取当前路由的所有父级别ID
        $list    = \app\common\model\AuthRule::getList();
        $praents = getParents($list, $rule['id']);
        $ids     = array();
        if (!empty($praents)) {
            foreach ($praents as $v) {
                array_push($ids, $v['id']);
            }
        }

        // 遍历数组 增加active标识
        $new_list = array();
        foreach($list as $v) {
            // 给父级ID添加ACTIVE
            if(in_array($v['id'], $ids)) {
                $v['active'] = 1; // 该栏目是否高亮
                $v['class']  = $class; // 导航高亮
            } else {
                $v['active'] = 0;
                $v['class']  = '';
            }

            // 生成URL
            if ($v['module'] != '#'){
                $v['url'] = url($v['module'] . DS . $v['controller'] . DS . $v['action']);
            } else {
                $v['url'] = '#';
            }

            // 组合新的数组
            if ($v['is_menu'] == 1){
                array_push($new_list, $v);
            }
        }

        // 格式化多维数组
        return list_to_tree($new_list, $pk='id', $pid = 'parent_id', $child = 'child', $root = 0);
    }

}