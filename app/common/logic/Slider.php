<?php
namespace app\common\logic;

use think\Model;
use app\common\model\Slider as SliderModel;

class Slider extends Model
{
    /**
     * 查询显示的幻灯片
     *
     * @params $map array 查询条件
     * @return array
     */
    public function getDisplay()
    {
        $list = SliderModel::getDisplay();

        $new_list = array();
        if (!empty($list)) {
            foreach ($list as $v){
                $v['image'] = buildImageUrl($v['image']);
                array_push($new_list, $v);
            }
        }

        return $new_list;
    }

    /**
     * 查询幻灯片列表
     *
     * @params $map array 查询条件
     * @return array
     */
    public function getList($map = array())
    {
        $list = SliderModel::getList($map);

        $new_list = array();
        if (!empty($list)) {
            foreach ($list as $v){
                $v['image'] = buildImageUrl($v['image']);
                array_push($new_list, $v);
            }
        }

        $data['list'] = $new_list;
        $data['page'] = $list->render();
        return $data;
    }

    /**
     * 查询幻灯片信息
     *
     * @params $id int ID
     * @return array
     */
    public function getInfo($id)
    {
        $info = SliderModel::getInfo($id);

        if (is_array($info)) {
            if (!empty($info['image'])) {
                $info['image'] = buildImageUrl($info['image']);
            }
        }

        return $info;
    }
}