<?php
namespace app\common\logic;

use think\Model;
use think\Loader;
use app\common\validate\UserValidate;
use app\common\model\User as UserModel;
use app\common\model\AuthGroup;
use app\common\model\Article as ArticleModel;

class User extends Model
{
    /**
     * 获取用户详情信息
     *
     * @params $uid int 用户ID
     * @return array|boolean
     */
    public function getUserInfo($uid = '')
    {
        if (empty($uid) || $uid == 'null') {
            $userAuth = session('user_auth');
            $uid = $userAuth['uid'];
        }

        $user = UserModel::getInfo($uid);
        if (!empty($user['avatar'])) {
            $user['avatar'] = getDomain() . $user['avatar'];
        }

        unset($user['password']);

        // 查询未读信息数量
        $count = \app\common\model\Message::getMessageCount($user['uid']);
        $user['message_count'] = $count ? $count : 0;
        
        // 用户文章总数和用户文章总浏览量
        $user['article_sum'] = ArticleModel::getArticleSum($uid);
        $user['article_pv_sum'] = ArticleModel::getArticlePvSum($uid);

        return $user;
    }
    
    /**
     * 保存用户信息
     *
     * @params data array 用户数据
     * @return int
     */
    public function add($data)
    {
        $validate = Loader::validate('UserValidate');
        $result = $validate->scene('add')->check($data);
        if (!$result) {
            return $validate->getError();
        }

        $data['create_at'] = date('Y-m-d H:i:s', time());
        $data['update_at'] = date('Y-m-d H:i:s', time());
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

        //删除不需要保存的字段
        unset($data['confirm_password']);

        //写入数据
        return UserModel::insertGetId($data);
    }

    /**
     * 更新用户信息
     *
     * @params $data array 用户数据
     * @return int
     */
    public function modify($data)
    {
        $validate = Loader::validate('UserValidate');
        $result = $validate->scene('edit')->check($data);
        $result = $validate->check($data);
        if (!$result) {
            return $validate->getError();
        }

        $data['update_at'] = date('Y-m-d H:i:s', time());

        // 删除不需要的字段 old_password
        if (isset($data['old_password'])) {
            unset($data['old_password']);
        }

        // 删除不需要的字段 confirm_password
        if (isset($data['confirm_password'])) {
            unset($data['confirm_password']);
        }

        // 密码为空则不更新
        if (! empty($data['password'])) {
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        } else{
            unset($data['password']);
        }

        return UserModel::modify($data);
    }

    /**
     * 校验原密码
     *
     * @params $uid int 用户uid
     * @return boolean
     */
    public static function checkPassword($uid, $newPassword)
    {
        if (empty($uid) || empty($newPassword)) {
            return false;
        }

        // 查询原始信息
        $oldPassword = UserModel::getPassword($uid);

        if (!empty($oldPassword)) {
            return password_verify($newPassword, $oldPassword) ? true : false;
        } else {
            return false;
        }
    }
}