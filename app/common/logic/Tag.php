<?php
namespace app\common\logic;

use think\Model;
use app\common\model\Tag as TagModel;

class Tag extends Model
{
    /**
     * 增加TAG
     *
     * @params $tag sting tag名称
     * @params $article_id int 文章ID
     * @return bool
     */
    public function add($tag, $article_id)
    {
        // 遍历数组 绑定TAG和文章的关系
        if (! empty($this->parse($tag)) && ! empty($article_id)) {
            foreach ($this->parse($tag) as $v) {
                $this->bind($v, $article_id);
            }

        } else {

            return false;
        }

        return true;
    }

    /**
     * 更新TAG
     *
     * @params $tag sting tag名称
     * @params $article_id int 文章ID
     * @return bool
     */
    public function modify($tag, $article_id)
    {
        if (! empty($this->parse($tag)) && ! empty($article_id)) {

            // 清理原有TAG关系
            TagModel::removeAccess($article_id);

            // 遍历数组 绑定TAG和文章的关系
            foreach ($this->parse($tag) as $v) {
                $this->bind($v, $article_id);
            }

        } else {

            return false;
        }

        return true;
    }

    /**
     * 绑定TAG关系
     *
     * @params $tag sting tag
     * @return array
     */
    public function bind($tag, $article_id)
    {
        // 如果存在TAG直接绑定关系
        $exist = TagModel::findTag($tag);
        if (! $exist) {

            $tag_id = TagModel::addTag($tag);
        } else {

            $tag_id = $exist['id'];
        }

        TagModel::bind($tag_id, $article_id);
    }

    /**
     * 解析TAG
     *
     * @params $tag sting tag
     * @return array
     */
    public function parse($tag = '')
    {
        $arr = [];

        // 解析TAG 规则:多个TAG使用英文','隔开
        if (! empty($tag)) {
            $str = str_replace('，', ',', trim($tag));
            $arr = explode(',', $str);
        }

        return $arr;
    }
}