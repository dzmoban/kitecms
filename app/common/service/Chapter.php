<?php
namespace app\common\service;

use think\Request;
use app\common\model\Chapter as ChapterModel;
use app\common\service\ServiceBase;

class Chapter extends ServiceBase
{
    /**
     * 章节信息
     *
     * @params $id int 章节ID
     * @return array
     */
    public function getInfo($id)
    {
        // 查询章节信息
        $data = ChapterModel::getInfo($id);

        // 查询下一条信息
        $next = ChapterModel::getNext($id);
        $data['next_url'] = !empty($next['id']) ? url('index/chapter/read', ['id' => $next['id']]) : '';
        $data['next_name'] = !empty($next['title']) ? $next['title'] : '';

        // 查询上一条信息
        $prev = ChapterModel::getPrev($id);
        $data['prev_url'] = !empty($prev['id']) ? url('index/chapter/read', ['id' => $prev['id']]) : '';
        $data['prev_name'] = !empty($prev['title']) ? $prev['title'] : '';

        // 目录页链接
        $data['book_url'] = url('index/book/read#tab_2', ['id' => $data['book_id']]);

        return $data;
    }

    /**
     * 章节列表(不分页)
     *
     * @params $map array 查询条件
     * @params $order array|string 排序规则
     * @return array
     */
    public function getAll($map, $order)
    {
        $list = ChapterModel::getAll($map, $order);
        $new_list = [];
        if (is_array($list)){
            foreach ($list as $v) {
                $v['chapter_url'] = url('index/chapter/read', ['id' => $v['id']]);
                array_push($new_list, $v);
            }
        }

        return $new_list;
    }
}