<?php
namespace app\common\service;

use think\Request;
use app\common\model\Tag as TagModel;
use app\common\model\User as UserModel;
use app\common\model\Category as CategoryModel;
use app\common\model\Comment as CommentModel;
use app\common\service\ServiceBase;

class Tag extends ServiceBase
{
    /**
     * 获取TAG列表
     *
     * @params $article int    文章ID
     * @return array
     */
    public function getList($order, $limit)
    {
        $map = array();
        $list = TagModel::getList($map, $order, $limit);
        $data = array();
        if (!empty($list)) {
            foreach ($list as $v) {
                $v['tag_url'] = url('index/tag/read', ['id' => $v['id']]);
                array_push($data, $v);
            }
        }
        return $data;
    }

    /**
     * 获取TAG
     *
     * @params $article int 文章ID
     * @return array
     */
    public function getTag($article_id)
    {
        if (empty($article_id) || $article_id == 'null') {
            $article_id = Request::instance()->param('id');
        }

        $tag = TagModel::getTag($article_id);
        $data = array();
        if (!empty($tag)) {
            foreach ($tag as $v) {
                $v['tag_url'] = url('index/tag/read', ['id' => $v['id']]);
                array_push($data, $v);
            }
        }
        return $data;
    }

    /**
     * 根据文章TagID获取相关文章
     *
     * @params $article_id  int  文章ID
     * @params $limit       int  数量
     * @return array 文章ID集合
     */
    public function getTagRelation($article_id, $limit)
    {
        if (empty($article_id) || $article_id == 'null') {
            $article_id = Request::instance()->param('id');
        }

        // 获取TagID集合
        $tags = TagModel::getTag($article_id);
        $tagIds = array();
        if (!empty($tags)) {
            foreach($tags as $v) {
                array_push($tagIds, $v['id']);
            }
        }

        // 查询文章ID
        $articleIds = array();
        if (!empty($tagIds)) {
            $map['tag_id'] = array('in', $tagIds);
            $list = TagModel::getTagRelation($map, $limit);
            if (!empty($list)) {
                foreach ($list as $v) {
                    if ($v['article_id'] != $article_id) {
                        array_push($articleIds, $v['article_id']);
                    }
                }
            }
        }

        return $articleIds;
    }

    /**
     * 增加TAG
     *
     * @params $article int    文章ID
     * @params $tagName string tag名
     * @return array
     */
    public function addTag($tagName, $article_id)
    {
        // 查询TAG是否存在 如果存在只更新引用数量
        $tag = TagModel::findTag($tagName);
        if (!empty($tag)) {
            // 更新引用数量
            $set_num = TagModel::setTagNum($tag['id'], $tag['num'] + 1);
            $tag_id = $tag['id'];
        } else {
            // 查询不到则新增TAG
            $tag_id = TagModel::addTag($tagName);
        }
        $bind = TagModel::bind($tag_id, $article_id);

        return $bind ? true : false;
    }

    /**
     * 根据Tag id获取文章列表
     *
     * @params $tag_id int ID
     * @params $order mix 排序规则
     * @params $limit int 每页数量
     * @return array|boolean
     */
    public function getArticleList($tag_id, $order, $limit)
    {
        if (empty($tag_id) || $tag_id == 'null') {
            $tag_id = Request::instance()->param('id');
        }

        $map = array(
            't.tag_id' => $tag_id,
            'a.status' => 1,
        );
        $list = TagModel::getArticleList($map, $order, $limit);
        $data['page'] = $list->render();
        $data['list'] = $this->formatArticle($list);
        return $data;
    }
}