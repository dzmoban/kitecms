<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Slider extends Model
{
    public static function getList($map)
    {
        return Db::name('slider')->where($map)->order('id desc')->paginate(15);
    }

    public static function getDisplay()
    {
        return Db::name('slider')->where('status', '<>', 1)->order('id desc')->select();
    }

    public static function getInfo($id)
    {
        return Db::name('slider')->where('id', $id)->find();
    }

    public static function add($data)
    {
        return Db::name('slider')->insertGetId($data);
    }

    public static function edit($data)
    {
        return Db::name('slider')->update($data);
    }
    
    public static function remove($id)
    {
        return Db::name('slider')->delete($id);
    }

    public static function setImage($id, $value)
    {
        return Db::name('slider')->where('id',$id)->setField('image', $value);
    }

}