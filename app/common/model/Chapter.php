<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Chapter extends Model
{
    /**
     * 详情
     *
     * @params $id int ID
     * @return array
     */
    public static function getInfo($id)
    {
        return Db::name('book_chapter')
            ->alias('c')
            ->field('c.*, b.title as book_name, b.author, c.title as section_name')
            ->where('c.id',$id)
            ->join('__BOOK__ b','b.id = c.book_id','LEFT')
            ->join('__BOOK_SECTION__ s','s.id = c.section_id','LEFT')
            ->find();
    }

    /**
     * 列表
     *
     * @params $map array 查询条件
     * @params $order array|string 排序
     * @params $limit int 每页数量
     * @return array
     */
    public static function getList($map, $order, $limit)
    {
        return Db::name('book_chapter')
            ->alias('c')
            ->field('c.*, b.title as book_name, s.title as section_name')
            ->where($map)
            ->join('__BOOK__ b','b.id = c.book_id','LEFT')
            ->join('__BOOK_SECTION__ s','s.id = c.section_id','LEFT')
            ->order($order)
            ->paginate($limit);
    }

    /**
     * 列表 (不分页)
     *
     * @params $map array 查询条件
     * @params $order array|string 排序
     * @return array
     */
    public static function getAll($map, $order)
    {
        return Db::name('book_chapter')
            ->alias('c')
            ->field('c.*, b.title as book_name')
            ->where($map)
            ->join('__BOOK__ b','b.id = c.book_id','LEFT')
            ->order($order)
            ->select();
    }

    /**
     * 创建
     *
     * @params $data array
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('book_chapter')->insertGetId($data);
    }

    /**
     * 更新
     *
     * @params $data array
     * @return mix
     */
    public static function modify($data)
    {
        return Db::name('book_chapter')->update($data);
    }

    /**
     * 删除
     *
     * @params $id int ID
     * @return int
     */
    public static function remove($id)
    {
        return Db::name('book_chapter')->delete($id);
    }

    /**
     * 获取下一条信息
     *
     * @params $id int ID
     * @return array
     */
    public static function getNext($id)
    {
        return Db::name('book_chapter')
            ->where('id', '>', $id)
            ->order('id asc')
            ->find();
    }

    /**
     * 获取下一条信息
     *
     * @params $id int ID
     * @return array
     */
    public static function getPrev($id)
    {
        return Db::name('book_chapter')
            ->where('id', '<', $id)
            ->order('id desc')
            ->find();
    }
}
