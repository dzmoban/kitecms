<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Message extends Model
{
    /**
     * 获取详情信息
     *
     * @params $message_id int ID
     * @return array
     */
    public static function getInfo($message_id)
    {
        return Db::name('message')
            ->alias('m')
            ->field('m.*, u.username, u.nickname')
            ->where('id', $message_id)
            ->join('__USER__ u', 'm.from_uid = u.uid', 'LEFT')
            ->find();
    }

    /**
     * 获取列表
     *
     * @params $map array 查询条件
     * @params $order array|string 排序方式
     * @params $limit int 数量
     * @return array
     */
    public static function getList($map, $order, $limit)
    {
        return Db::name('message')
            ->alias('m')
            ->field('m.*, u.username')
            ->where($map)
            ->join('__USER__ u', 'm.from_uid = u.uid', 'LEFT')
            ->order($order)
            ->paginate($limit, true);
    }

    /**
     * 统计未读私信数量
     *
     * @params $uid int UID
     * @return int
     */
    public static function getMessageCount($uid)
    {
        return Db::name('message')
            ->where('to_uid', '=', $uid)
            ->where('status', '=', 0)
            ->count();
    }

    /**
     * 发送信息
     *
     * @params $data array 消息数据
     * @return int
     */
    public static function sendMessage($data)
    {
        return Db::name('message')->insertGetId($data);
    }

    /**
     * 设置信息状态
     *
     * @params $message_id int 消息ID编号
     * @return boolean
     */
    public static function setStatus($message_id)
    {
        return Db::name('message')
            ->where('id', '=', $message_id)
            ->setField('status', '1');
    }
}