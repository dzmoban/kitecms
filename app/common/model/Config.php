<?php
namespace app\common\model;

use think\Model;
use think\Db;
use think\Cache;

class Config extends Model
{
    // 配置列表
    public static function getList()
    {
        return Db::name('config')->order('id desc')->paginate(10);
    }

    // 获取配置组列表
    public static function getGroupList($flag)
    {
        return Db::name('config')->where('flag', $flag)->order('reorder desc')->select();
    }

    // 设置配置
    public static function setConfig($data)
    {
        return Db::name('config')->insert($data);
    }

    // 设置value
    public static function setValue($key, $vlue){
        return Db::name('config')->where('key', $key)->setField('value', $vlue);
    }

    // 更新数据
    public static function edit($data)
    {
        return Db::name('config')->update($data);
    }

    // 删除数据
    public static function remove($id)
    {
        return Db::name('config')->delete($id);
    }

    // 获取配置
    public static function getConfig($key)
    {
        return Db::name('config')->where('key', '=', $key)->find();
    }

    // 获取配置 根据ID
    public static function getInfo($id)
    {
        return Db::name('config')->where('id', '=', $id)->find();
    }

    // 获取配置值
    public static function getValue($key)
    {
        return Db::name('config')->where('key', $key)->value('value');
    }

}