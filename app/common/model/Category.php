<?php
namespace app\common\model;

use think\Model;
use think\Db;
use app\common\model\Module;

class Category extends Model
{
    /**
     * 获取栏目信息
     *
     * @params $id int ID编号
     * @return array
     */

    public static function getInfo($id)
    {
        return Db::name('category')
            ->alias('c')
            ->field('c.*, m.name as module_name , m.table_name')
            ->join('__MODULE__ m', 'c.module_id = m.id', 'LEFT')
            ->where('c.id', '=', $id)
            ->find();
    }

    /**
     * 根据别名获取栏目信息
     *
     * @params $alias string 别名
     * @return array
     */

    public static function getInfoByAlias($alias)
    {
        return Db::name('category')
            ->alias('c')
            ->field('c.*, m.name as module_name , m.table_name')
            ->join('__MODULE__ m', 'c.module_id = m.id', 'LEFT')
            ->where('c.alias', '=', $alias)
            ->find();
    }

    /**
     * 获取栏目列表
     *
     * @params $map mix
     * @return array
     */
    public static function getList($map, $order, $limit)
    {
        return $list = Db::name('category')
            ->alias('c')
            ->field('c.*, m.name as module_name , m.table_name')
            ->join('__MODULE__ m', 'c.module_id = m.id', 'LEFT')
            ->where($map)
            ->order($order)
            ->limit($limit)
            ->select();
    }

    /**
     * 获取子栏目列表
     *
     * @params $parent_id int 上级ID
     * @return array
     */
    public static function getChildren($parent_id)
    {
        return Db::name('category')
            ->alias('c')
            ->field('c.*, m.name as module_name , m.table_name')
            ->join('__MODULE__ m', 'c.module_id = m.id', 'LEFT')
            ->where('parent_id', '=', $parent_id)
            ->select();
    }

    /**
     * 获取上级栏目信息
     *
     * @params $id int 当前ID
     * @return array
     */
    public static function getParentID($id)
    {
        return Db::name('category')
            ->alias('c')
            ->field('c.*, m.name as module_name , m.table_name')
            ->join('__MODULE__ m', 'c.module_id = m.id', 'LEFT')
            ->where('c.id', 'eq', $id)
            ->find();
    }

    /**
     * 创建栏目
     *
     * @params $data array
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('category')->insertGetId($data);
    }

    /**
     * 栏目更新
     *
     * @params $data array
     * @return mix
    */
    public static function edit($data)
    {
        return Db::name('category')->update($data);
    }

    /**
     * 根据英文别名获取ID
     
     * @params $alias sting 别名
     * @return int
     */
    public static function getId($alias)
    {
        return Db::name('category')->where('alias', '=', $alias)->value('id');
    }

    /*
     * 根据ID获取英文别名
     * @params $id int ID
     * @return string
    */
    public static function getAlias($category_id)
    {
        return Db::name('category')->where('id', '=', $category_id)->value('alias');
    }

    /**
     * 根据分类ID获取分类名称
     *
     * @params $id category_id 分类ID
     * @return string|boolean
     */
    public static function getName($category_id)
    {
        return Db::name('category')->where('id', '=', $category_id)->value('title');
    }

    /**
     * 根据英文别名判断栏目是否已经存在
     *
     * @params $alias sting 别名
     * @return mix
    */
    public static function checkEnName($alias)
    {
        return Db::name('category')->where('alias', $alias)->find();
    }

    /**
     * 检测栏目是否有子栏目
     * 栏目下有子栏目 不允许删除
     *
     * @params $id int ID编号
     * @return mix
     */
    public static function checkSubclassification($id)
    {
        //检测子栏目
        return Db::name('category')->where('parent_id', $id)->limit(0, 1)->select();
    }

    /**
     * 检测栏目下内容
     * 栏目下有内容 不允许删除
     
     * @params $id 
     * @return mix
     */
    public static function checkContent($id)
    {
        //查询栏目信息
        $cateInfo = self::getInfo($id);
        $modouleInfo = Module::getInfo($cateInfo['module_id']);
        if ($modouleInfo['table_name'] == 'page') {
            return false;
        }

        $ret = Db::name($modouleInfo['table_name'])
            ->where('category_id', $id)
            ->limit(0, 1)
            ->select();
        return $ret;
    }

    /**
     * 删除
     *
     * @params $id int 分类ID
     * @return int
     */
    public static function remove($id)
    {
        return Db::name('category')->delete($id);
    }

    /**
     * 排序
     *
     * @params $id int 分类ID
     * @params $order int 排序数值
     * @return int
     */
    public static function reorder($id, $order)
    {
        return Db::name('category')->where('id', $id)->setField('reorder', $order);
    }
}