<?php
namespace app\common\behavior;

use think\Controller;
use think\Hook;

class InitHook extends Controller
{
    //初始化钩子信息
    public function run(&$params)
    {
        // 检验权限
        Hook::add('action_begin',['app\\common\\behavior\\CheckAuth']);

        // 登陆记录日志
        Hook::add('login',['app\\common\\behavior\\UserLoginLog']);
    }
}