<?php
namespace app\index\controller;

use think\Request;
use app\common\logic\Passport as PassportLogic;
use app\common\service\Message as MessageLogic;
use app\common\service\Article as ArticleLogic;
use app\common\service\Comment as CommentLogic;
use app\common\service\Favorite as FavoriteLogic;
use app\common\logic\User as UserLogic;
use app\index\controller\Base;

class Member extends Base
{
    // 前置操作
    protected $beforeActionList = [
        'checkLogin' => ['except' => 'login,logout,register,forget,profile'],
    ];

    /** 
     * 校验登陆
     *
     */
    public function checkLogin()
    {
        if (!is_login()) {
            $this->error('请先登录', 'index/member/login');
        }
    }

    /** 
     * 我的消息
     *
     */
    public function message()
    {
        //查询数据
        $map = array(
            'to_uid' => $this->uid,
        );
        $order = array(
            'status' => 'asc',
            'create_at' => 'desc',
        );

        $messageOjb = new MessageLogic();
        $list = $messageOjb->getList($map, $order, $this->config('message_limit'));

        $this->assign('list', $list['list']);
        $this->assign('page', $list['page']);

        return $this->fetch($this->template('message_tpl'));
    }

    /** 
     * 阅读消息
     *
     */
    public function readMessage()
    {
        $message_id = Request::instance()->param('id');
        $messageOjb = new MessageLogic();
        $data = $messageOjb->getInfo($message_id);
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->assign($k, $v);
            }
        } else {
            $this->errer('您要查阅的消息不存在!');
        }

        //设置已经阅读
        if ($data['status'] != 1){
            \app\common\model\Message::setStatus($message_id);
        }

        return $this->fetch($this->template('read_message_tpl'));
    }

    /** 
     * 我的文章
     *
     */
    public function article()
    {
        $articleLogic = new ArticleLogic();
        $data = $articleLogic->myArticle($this->uid, $this->config('article_limit'));

        $this->assign('list', $data['list']);
        $this->assign('page', $data['page']);

        return $this->fetch($this->template('my_article_tpl'));
    }

    /** 
     * 我的评论
     *
     */
    public function comment()
    {
        $commentLogic = new CommentLogic();
        $data = $commentLogic->myComment($this->uid, $this->config('comment_limit'));

        $this->assign('list', $data['list']);
        $this->assign('page', $data['page']);
        return $this->fetch($this->template('my_comment_tpl'));
    }

    /** 
     * 我的收藏
     *
     */
    public function favorite()
    {
        $favoriteLogic = new FavoriteLogic();

        // 收藏的文章列表
        $article = $favoriteLogic->getArticleList($this->uid, $this->config('article_limit'));

        // 收藏的图书列表
        $book = $favoriteLogic->getBookList($this->uid, $this->config('book_limit'));

        $this->assign('article', $article);
        $this->assign('book', $book);
        return $this->fetch($this->template('my_favorite_tpl'));
    }

    /** 
     * 会员资料
     *
     */
    public function setting()
    {
        $ojb = new UserLogic();
        $data = $ojb->getUserInfo($this->uid);

        $this->assign('data', $data);
        return $this->fetch($this->template('my_setting_tpl'));
    }

    /** 
     * 登陆
     *
     */
    public function login()
    {
        return $this->fetch($this->template('login_tpl'));
    }

    /** 
     * 登出
     *
     */
    public function logout()
    {
        $model = new PassportLogic();
        $model->clear();
        $this->redirect(url('index/member/login'),302);
    }

    /** 
     * 注册
     *
     */
    public function register()
    {
        return $this->fetch($this->template('register_tpl'));
    }

    /** 
     * 找回密码
     *
     */
    public function forget()
    {
        return $this->fetch($this->template('forget_tpl'));
    }


    /** 
     * 会员详情
     *
     */
    public function profile()
    {
        $uid = Request::instance()->param('uid');
        // 查询用户信息
        $userLogic = new UserLogic();
        $userInfo = $userLogic->getUserInfo($uid);

        // 查询用户文章列表
        $articleLogic = new ArticleLogic();
        $articleList = $articleLogic->getAll(['a.uid' => $uid], 'id desc', $this->config('article_limit'));

        $this->assign('user', $userInfo);
        $this->assign('article', $articleList);
        return $this->fetch($this->template('member_tpl'));
    }
}
