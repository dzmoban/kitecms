<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2015 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------

namespace app\api\controller;

use think\Config;
use app\api\controller\ApiBase;
use verify\Captcha as CaptchaTool;
use app\common\model\Config as ConfigModel;

class Captcha extends ApiBase
{
    public function index($id = "")
    {
        //查询数据库验证码配置信息
        $str = ConfigModel::getValue('captcha');
        $config = json_decode($str, true);

        //如果数据库没有配置 则使用默认config配置
        if (empty($config)) {
            $config = Config::get('captcha');
        }

        $captcha = new CaptchaTool((array)$config);
        return $captcha->entry($id);
    }
}