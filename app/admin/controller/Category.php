<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;
use app\common\model\Category as CategoryModel;
use app\common\logic\Category as CategoryLogic;
use app\common\model\Module;

class Category extends Admin
{
    private $model = null;

    function _initialize()
    {
        $this->model = new CategoryLogic();
    }

    public function index()
    {
        $data = $this->model->getList();
        $this->assign('data', $data);
        return $this->fetch('admin/category/index');
    }

    public function create()
    {
        $data['category'] = $this->model->getList();
        // 获取模型列表
        $data['module'] = Module::getList();
        // 获取模版列表
        $data['template'] = $this->templateList();
        $this->assign('data', $data);
        return $this->fetch('admin/category/create');
    }

    public function store()
    {
        $data = Request::instance()->param();

        // 分类名称和别名不能为空
        if (!$data['title'] || !$data['alias']) {
            return $this->response(1205);
        }

        // 根据别名判断是否重复
        $checkEnName = CategoryModel::checkEnName($data['alias']);
        if ($checkEnName) {
            return $this->response(1202);
        }

        $result = $this->model->add($data);
        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(222, $result);
        }
    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $data = CategoryModel::getInfo($id);

        $map['c.id'] = array('<>', $id);
        $data['category'] = $this->model->getList($map);

        // 获取模型列表
        $data['module'] = Module::getList();

        // 获取模版列表
        $data['template'] = $this->templateList();
        
        $this->assign('data', $data);
        return $this->fetch('admin/category/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        $info = CategoryModel::getInfo($data['id']);

        if ($data['alias'] != $info['alias']) {
            $checkEnName = CategoryModel::checkEnName($data['alias']);
            if ($checkEnName) {
                return $this->response(1202);
            }
        }

        $result = $this->model->edit($data);

        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(222, $result);
        }
    }

    public function remove()
    {
        $id = Request::instance()->param('id');

        // 检查该栏目是否拥有子栏目
        $checkSon = CategoryModel::checkSubclassification($id);
        if ($checkSon) {
            return $this->response(1204);
        }

        // 检查栏目下是否有内容
        $checkContent = CategoryModel::checkContent($id);
        if ($checkContent) {
            return $this->response(1203);
        }

        // 删除栏目
        $result = CategoryModel::remove($id);
        if ($result !== false) {
            return $this->response(200);
        }
        return $this->response(201);
    }

    public function reorder()
    {
        $data = Request::instance()->param();
        $list = array_combine($data['ids'],$data['reorder']);
        foreach($list as $k=>$v){
            $result = CategoryModel::reorder($k, $v);
        }

        if ($result !== false) {
            return $this->response(200);
        }

        return $this->response(201);
    }
}
